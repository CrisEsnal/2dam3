import java.util.concurrent.Semaphore;

public class EjercicioFilosofos{
    
}
class Filosofo extends Thread{
    private boolean comiendo;
    private Semaphore der;
    private Semaphore izq;
    private int id;
    private int espaguetis;
    Filosofo(Semaphore der,Semaphore izq,int id){
        this.der = der;
        this.izq = izq;
        this.id = id;
        this.espaguetis = 0;
    }
    public synchronized boolean puedeComer(){
        
        if(izq.availablePermits()==1 && der.availablePermits()==1){
            return true;
        }else{
            return false;
        }
    }
    public synchronized void run(){
        for (int i = 0; i<10;i++){
            
            try{
                Thread.sleep((long)((Math.random()*10000)+1));
            }catch(InterruptedException e){
                e.printStackTrace();
            }
            if(puedeComer()){
                try{
                    izq.acquire();
                    der.acquire();
                    System.out.println("Come" + this.id);
                    Thread.sleep(1000);
                    izq.release();
                    //Thread.sleep(1000);
                    der.release();
                    System.out.println("Filosofo "+this.id+" ha soltado los tenedores");
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
            }
            
        }
    }
}