import java.util.LinkedList;
import java.util.concurrent.Semaphore;

public class Barberia extends Thread{
	private Semaphore semSillas;
	private LinkedList<Cliente> arrayCli = new LinkedList<>();
	
	private Silla silla = new Silla();

	public Barberia() {
		this.semSillas = new Semaphore(5);
	}
	
	public void entrarBarberia(Cliente cli) {
		if(arrayCli.size()<=4) {
			irSEspera(cli);
		}else {
			System.out.println("La barberia esta llena, cliente  "+cli.getNumCli()+" se ha ido");
		}
	}
	public synchronized void irSEspera(Cliente cli) {
		try {
			semSillas.acquire();
			arrayCli.add(cli);
			Main.asientoPonX(cli);
			//System.out.println("Cliente"+ cli.getNumCli() +" se sienta en espera");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public Cliente sigCli() {
		Cliente cli = arrayCli.poll();	
		Main.asientoQuitaX(cli);
		//System.out.println("Siguiente Cliente en sentarse: " + cli.getNumCli() );
		semSillas.release();
		return cli;
	}
	public synchronized void gastarPaciencia(Cliente cli) {
		
		System.out.println("El cliente "+cli.getNumCli()+" Se ha ido por falta de paciencia");
		semSillas.release();
		arrayCli.remove(cli);
		Main.asientoQuitaX(cli);
		Main.imprimir();
	}
	public LinkedList<Cliente> getClientes() {
		return arrayCli;
	}
	public Silla getSilla() {
		return silla;
	}

	public void setSilla(Silla silla) {
		this.silla = silla;
	}
	

}
