import java.util.HashMap;
import java.util.Map;

public class Main {
	public static String[] asientosEspera = new String[5];
	public static String[] asientoBarbero = new String[2];	
	public static Map<Cliente, Integer> map = new HashMap<Cliente, Integer>();
	public static String log;

	public static void main(String[] args) {
		for (int i = 0; i < asientosEspera.length; i++) {
			asientosEspera[i] = "-";
		}	
		asientoBarbero[0] = "-";
		asientoBarbero[1] = "B";

		int nSillas = 5;
		Barberia barberia = new Barberia();
		Barbero barbero = new Barbero(barberia);
		crearCli crearCliente = new crearCli(barberia, barbero);
		barbero.start();
		crearCliente.start();
	}
	public synchronized static void asientoPonX(Cliente cliente) {
		int i = 0;
		while(!asientosEspera[i].equals("-")) { // Busca un asiento donde sentarse
			i++;
		}
		map.put(cliente, i);
		//System.out.println("--->"+map.get(cliente));
		asientosEspera[i] = ""+cliente.getNumCli();
		log = "Se sienta un cliente";
		imprimir(); // Cada vez que el array se modifica se imprime todo
	}
	public synchronized static void imprimir() {
		System.out.print(" | ");
		for(String asiento : asientosEspera) {
			System.out.print(asiento + " | ");
		}
		System.out.print(" ----------- ");
		
		System.out.print(" | ");
		for(String asientoBarbero : asientoBarbero) {
			System.out.print(asientoBarbero + " | ");
		}
		System.out.print(log);
		System.out.println();
		log="";

	}

	public synchronized static void asientoQuitaX(Cliente cliente) {
		//System.out.println(map.get(cliente));
		asientosEspera[map.get(cliente)] = "-"; // obtengo la posicion donde se ha sentado ese cliente y le vaco el asiento
		map.remove(cliente);
	}
	
	public synchronized static void barberoPonX(String asiento, String barbero, String estado) {
    	Main.asientoBarbero[0] = asiento;
    	Main.asientoBarbero[1] = barbero;
    	
    	log = estado;
		imprimir(); // Cada vez que el array se modifica se imprime todo
	}


}
