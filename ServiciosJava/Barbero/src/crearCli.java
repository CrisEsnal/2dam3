
public class crearCli extends Thread{
	private Barberia barberia;
	private Barbero barbero;
	private int numCliente=0;

	
	crearCli(Barberia barberia, Barbero barbero) {
		this.barberia = barberia;
		this.barbero = barbero;
	}
	
	public void run() {
		while(true) {
			try {
				Thread cliente = new Thread(new Cliente(barberia, barbero,numCliente));				
				cliente.start();
				sleep((long) (Math.random()*10000));
				numCliente++;	
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
	}
}
