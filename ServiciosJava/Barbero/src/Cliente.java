
public class Cliente extends Thread{
	private int paciencia;
	private Barberia barberia;
	private Barbero barbero;
	private int numCli;
	
	Cliente(Barberia barberia,Barbero barbero,int numCli){
		this.barberia = barberia;
		this.barbero = barbero;
		this.paciencia = (int) (Math.random()*10000);
		this.numCli = numCli;
		
	}
	public void run() {
		barberia.entrarBarberia(this);
		if(barbero.getDormir()) {
			
			pacInit(this);
		}
	}
	public synchronized void pacInit(Cliente cliente) {
		try {
			cliente.wait(this.getPac());
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(barbero.getDormir()) {
			barberia.gastarPaciencia(this);
		}
	}
	public int getPac() {
		return paciencia;
	}
	public int getNumCli() {
			return numCli;
		}
}
