import java.util.concurrent.Semaphore;
public class Silla {
	Boolean sentado = false;
	Cliente cliente;
	Semaphore semaforo;
	
	Silla(){
		this.sentado=false;
		semaforo = new Semaphore(1);
	}
	
	public void sentar(){		
		try {
			semaforo.acquire();
		sentado = true;
		}catch(InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	public void levantar(){		
		sentado = false;
		semaforo.release();
	}
}
