
public class Barbero extends Thread {
	private Barberia barberia;
	private Cliente cliente;
	private Boolean dormir;
	
	Barbero(Barberia barberia){
		this.setDormir(false);
		this.barberia = barberia;
	}
	public Boolean getDormir() {
		return dormir;
	}

	public void setDormir(Boolean dormir) {
		this.dormir = dormir;
	}
	public void irseDormir() {
		try {
			this.setDormir(true);
			barberia.getSilla().sentar();
			Main.barberoPonX("B", "-", "Durmiendo");
			//System.out.println("El barbero se ha quedado frito");
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		despertar();

	}
	public void despertar() {
		this.setDormir(false);
		barberia.getSilla().levantar();
		Main.barberoPonX("-","B", "Despert�");
		//System.out.println("El barbero se ha levantado de la silla");
		for(Cliente cli : barberia.getClientes()){
	        synchronized (cli){
	        	cli.notify();	        	          
	        }
		}

	}
	public synchronized void cortar(Cliente cli) throws InterruptedException {
		Main.barberoPonX(""+cli.getNumCli(), "B", "Cortando barba");
		//System.out.println("El cliente n�mero " + cli.getNumCli() + " se ha sentado en el asiento del barbero");
		barberia.getSilla().sentar();
		sleep((long) (Math.random()*20000));
		Main.barberoPonX("-", "B", "Corte terminado");
		//System.out.println("Corte terminado Cliente "+cli.getNumCli()+" se levanta");
		barberia.getSilla().levantar();
	}
		
	public void run() {
		while (true) {
			
				if (barberia.getClientes().size()==0) {
					irseDormir();
				}else {
					try {
						cortar(barberia.sigCli());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			
		}
	}
	

}
