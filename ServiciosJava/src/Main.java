import java.util.concurrent.Semaphore;

public class Main {
	public static Semaphore tenedores[] = {new Semaphore(1),new Semaphore(1),new Semaphore(1),new Semaphore(1),new Semaphore(1)};
	public static Filosofo filosofos[] = {new Filosofo(tenedores[0],tenedores[1],0),new Filosofo(tenedores[1],tenedores[2],1),new Filosofo(tenedores[2],tenedores[3],2),new Filosofo(tenedores[3],tenedores[4],3),new Filosofo(tenedores[4],tenedores[0],4)};
	public static int comiendo[] = {0,0,0,0,0};
	public static void main(String[] args){
		
        for(int i = 0; i<filosofos.length;i++){
            filosofos[i].start();
        }
        
    }
}
