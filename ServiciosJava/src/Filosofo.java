import java.util.concurrent.Semaphore;

public class Filosofo extends Thread{
    private Semaphore der;
    private Semaphore izq;
    private int id;
    //private int espaguetis;
    Filosofo(Semaphore der,Semaphore izq,int id){
        this.der = der;
        this.izq = izq;
        this.id = id;
        //this.espaguetis = 0;
    }

public synchronized void imprimir(){
        String str = "[";
        for(int j = 0;j<Main.comiendo.length;j++){
            
            if(Main.comiendo[j]==1){
                str+="|x";
            }else if(id==j){
            	Main.comiendo[id] = 1;
                str+="|y";
            }
            else{
                str+="|.";
            }
            
        }
        str+="|]";
        System.out.println(str);
    }
    public synchronized boolean puedeComer(){
        
        if(izq.availablePermits()==1 && der.availablePermits()==1){
            return true;
        }else{
            return false;
        }
    }
    public void run(){
        for (int i = 0; i<10;i++){
            try{
                Thread.sleep((long)((Math.random()*10000)+1));
            }catch(InterruptedException e){
                e.printStackTrace();
            }
            if(puedeComer()){
                try{
                    izq.acquire();
                    der.acquire();
                    imprimir();
                    Thread.sleep(1000);
                    izq.release();
                    der.release();
                    Main.comiendo[id]=0;
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
            }
            
        }
    }
}