package com.example.animales;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String[] lista = getResources().getStringArray(R.array.ArrayAnim);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,lista );
        ListView listv = findViewById(R.id.listview);
        listv.setAdapter(arrayAdapter);
        final MediaPlayer[] sonidoks = {MediaPlayer.create(this, R.raw.espania),MediaPlayer.create(this, R.raw.gato),MediaPlayer.create(this, R.raw.perro),MediaPlayer.create(this, R.raw.caballo)};
        listv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String sonidos = getResources().getStringArray(R.array.ArraySonidos)[i];
                Toast toast1 = Toast.makeText(getApplicationContext(),sonidos,Toast.LENGTH_SHORT);
                toast1.show();
                sonidoks[i].start();

            }
        });
    }
}
