package com.example.server1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class MainActivity extends AppCompatActivity {
    Conexion conn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tv = findViewById(R.id.textView);
        final EditText et = findViewById(R.id.editText);
        Button bot = findViewById(R.id.button);
        Handler hiloPrincipal = new Handler();

        conn = new Conexion(hiloPrincipal,tv);
        conn.execute();
        bot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                conn.enviar(et.getText().toString());

            }
        });
        /*HiloHandle handle = new HiloHandle("miHandle");
        Runnable run = new Runnable() {
            @Override
            public void run() {
                while(true){
                    Log.d("Hilo1","1");
                }
            }
        };
        handle.start();
        HiloHandle handle2 = new HiloHandle("miHandle2");
        Runnable run2 = new Runnable() {
            @Override
            public void run() {
                while(true){
                    Log.d("Hilo1","1");
                }
            }
        };*/
        /*
        final Handler hiloPrincipal = new Handler(){
            @Override
            public void handleMessage(Message mensaje){
                super.handleMessage(mensaje);
                String msg = mensaje.getData().getString("Mensaje");
                escribir(msg);
            }
        };

        HandlerThread hilo1 = new HandlerThread("Hilo1");
        HandlerThread hilo2 = new HandlerThread("Hilo2");
        hilo1.start();
        hilo2.start();
        final Handler handler = new Handler(hilo1.getLooper());
        Handler handler2 = new Handler(hilo2.getLooper());


        Button bt = findViewById(R.id.button);
        final Socket sk = Conexion.crearSocket();
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            InetSocketAddress isa = new InetSocketAddress("10.0.2.2",4848);
                            sk.connect(isa);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
*/
        /*
        handler.post(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while(i<50){
                    Message msg = hiloPrincipal.obtainMessage();
                    Bundle bundle = new Bundle();
                    bundle.putString("Mensaje","Hilo1");
                    msg.setData(bundle);
                    hiloPrincipal.sendMessage(msg);
                    i++;
                }
            }
        });
        handler2.post(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while(i<50){
                    Message msg = hiloPrincipal.obtainMessage();
                    Bundle bundle = new Bundle();
                    bundle.putString("Mensaje","Hilo2");
                    msg.setData(bundle);
                    hiloPrincipal.sendMessage(msg);
                    i++;
                }
            }
        });
*/
    }
    public void escribir(String mensaje){
        TextView text = findViewById(R.id.textView);
        text.append(mensaje+"\n");

    }
}
