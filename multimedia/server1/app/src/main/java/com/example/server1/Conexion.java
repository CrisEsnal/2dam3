package com.example.server1;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

import androidx.core.os.HandlerCompat;

public class Conexion extends AsyncTask<Void,Void,Void> {
    OutputStream os;
    String t;
    Socket sk;
    Handler HandlerPrincipal;
    TextView tv;
    BufferedReader br;
   public Conexion(){

   }
   public Conexion(Handler handlerPrinc, TextView tv){
       HandlerPrincipal = handlerPrinc;
       this.tv = tv;
   }
    @Override
    protected Void doInBackground(Void... voids) {
        try {
            sk = new Socket("10.0.2.2",3333);
            os = sk.getOutputStream();
            br = new BufferedReader(new InputStreamReader(sk.getInputStream()));
            /*while((t = br.readLine()) != null){
                Log.e("Mensaje", t);
            }*/
            HandlerThread escucha = new HandlerThread("escucha");
            escucha.start();
            Looper loop =  escucha.getLooper();
            final Handler handler = new Handler(loop);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    try{
                        while((t = br.readLine())!= null){
                            HandlerPrincipal.post(new Runnable() {
                                @Override
                                public void run() {
                            Log.e("Escribir mensaje","Aqui tiene que escribirse el mensaje");
                                    tv.append("\n"+t);
                                }
                            });
                        }
                    }catch(Exception e){

                    }
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }



        return null;
    }
    public void enviar(String mensaje){
        try{
            Log.e("Mensaje","Enviado mensaje");
            mensaje =  mensaje + "\n";
            os.write(mensaje.getBytes());
            os.flush();
        }catch (Exception e){
            Log.e("NONO","error al enviar");
        }
    }


    /*public static Socket crearSocket(){
        Socket sk = null;
        try {
            sk = new Socket("10.0.2.2",4848);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sk;
    }*/
}
