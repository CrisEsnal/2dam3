package com.example.eltiempo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.renderscript.ScriptGroup;
import android.util.Base64;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Descarga extends AsyncTask<String,Void,Void> {
    InputStream in;
    Handler handler, handler2;
    Bitmap bm;
    URL urlImagen;
    InputStream inI;
    boolean mapa = false;

    public Descarga(Handler handler) {
        this.handler = handler;
        this.handler2 = handler;

    }

    @Override
    protected Void doInBackground(String... urls) {
        try {
            URL url = new URL("http://www.euskalmet.euskadi.eus/contenidos/prevision_tiempo/met_forecast/opendata/met_forecast.xml");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            in = new BufferedInputStream(conn.getInputStream());

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {



            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new InputStreamReader(in, "ISO-8859-1"));
            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {

                if (eventType == XmlPullParser.START_TAG && xpp.getName().equals("es")) {

                   // Log.e("yaboi",xpp.getAttributeName(0));
                    System.out.println("Start tag " + xpp.getName());
                    xpp.next();
                    enviar(xpp.getText(), null);
                }else if(mapa == false){
                    urlImagen = new URL("http://www.euskalmet.euskadi.eus/contenidos/recurso_tecnico/tdtrtc/es_web/images/mapaweb.jpg");
                    HttpURLConnection connix = (HttpURLConnection) urlImagen.openConnection();
                    connix.setDoInput(true);
                    connix.connect();
                    InputStream inIx = connix.getInputStream();
                    Bitmap bmpx = BitmapFactory.decodeStream(inIx);
                    enviar(null, bmpx);
                    mapa = true;
                } else if (eventType == XmlPullParser.START_TAG && xpp.getName().equals("symbolImage")) {
                    xpp.next();
                    System.out.println("IMAGEN " + xpp.getText());
                    urlImagen = new URL("http://www.euskalmet.euskadi.eus" + xpp.getText());
                    //Log.e("url","http://www.euskalmet.euskadi.eus"+xpp.getText());
                    HttpURLConnection conni = (HttpURLConnection) urlImagen.openConnection();
                    conni.setDoInput(true);
                    conni.connect();
                    InputStream inI = conni.getInputStream();
                    Bitmap bmp = BitmapFactory.decodeStream(inI);
                    enviar(null, bmp);
                }
                else if (eventType == XmlPullParser.START_TAG && xpp.getName().equals("forecast")) {
                    Log.e("yaboi",xpp.getAttributeValue(0));
                    System.out.println("Start tag " + xpp.getName());
                    xpp.next();
                }
                eventType = xpp.next();
            }
            System.out.println("End document");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void enviar(String a, Bitmap bmp) {

        final String b = a;
        if (bmp == null) {

            handler.post(new Runnable() {

                @Override
                public void run() {
                    Message msg = handler.obtainMessage();
                    Bundle bundle = new Bundle();
                    bundle.putString("mensaje", b);
                    msg.setData(bundle);
                    handler.sendMessage(msg);
                }
            });
        } else if (a == null) {

                Bitmap img = bmp;
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                img.compress(Bitmap.CompressFormat.JPEG, 100, out);
                byte[] byteArr = out.toByteArray();
                final String str64 = Base64.encodeToString(byteArr, 0);
                Log.e("ijijijij", str64);
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        Log.e("AQUI NO ENTRA", "NO ENTRA");
                        Message msg = handler.obtainMessage();
                        Bundle bundle = new Bundle();
                        bundle.putString("imagen", str64);
                        msg.setData(bundle);
                        handler.sendMessage(msg);
                    }
                });

        }
    }

    public void enviarImg(Bitmap bmp) {
        Bitmap img = bmp;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        img.compress(Bitmap.CompressFormat.JPEG, 100, out);
        byte[] byteArr = out.toByteArray();
        final String str64 = Base64.encodeToString(byteArr, 0);
        Log.e("ijijijij", str64);
        handler2.post(new Runnable() {

            @Override
            public void run() {
                Log.e("AQUI NO ENTRA", "NO ENTRA");
                Message msg = handler2.obtainMessage();
                Bundle bundle = new Bundle();
                bundle.putString("imagen", str64);
                msg.setData(bundle);
                handler2.sendMessage(msg);
            }
        });
    }

    /*public static Bitmap makeTransparent(Bitmap bit, Color transparentColor) {
        int width = bit.getWidth();
        int height = bit.getHeight();
        Bitmap myBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        int[] allpixels = new int[myBitmap.getHeight() * myBitmap.getWidth()];
        bit.getPixels(allpixels, 0, myBitmap.getWidth(), 0, 0, myBitmap.getWidth(), myBitmap.getHeight());
        myBitmap.setPixels(allpixels, 0, width, 0, 0, width, height);

        for (int i = 0; i < myBitmap.getHeight() * myBitmap.getWidth(); i++) {
            if (allpixels[i] == transparentColor)
                allpixels[i] = Color.alpha(Color.TRANSPARENT);
        }

        myBitmap.setPixels(allpixels, 0, myBitmap.getWidth(), 0, 0, myBitmap.getWidth(), myBitmap.getHeight());
        return myBitmap;

    }*/
}
