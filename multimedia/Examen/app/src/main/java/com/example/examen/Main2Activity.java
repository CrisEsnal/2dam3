package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {
    public static int intentos = 0;
    public static Boolean ganar = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intent = getIntent();
        String nombre = intent.getStringExtra(MainActivity.MENSAJE);
        TextView mensajeHola = findViewById(R.id.textView);
        mensajeHola.setText("Hola: "+nombre);
        Button boton = findViewById(R.id.jugar);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView numero = findViewById(R.id.numero);
                String num = numero.getText().toString();
                if(!ganar){
                    if(num.equals("")){
                        Toast alert = Toast.makeText(getApplicationContext(),"Introduce un digito",Toast.LENGTH_SHORT);
                        alert.show();
                    }else{
                        if(intentos<10){
                            if(verificar(num)){
                                ganar = true;
                                Toast mensajeganar = Toast.makeText(getApplicationContext(),"Has ganado el resultado era 2",Toast.LENGTH_SHORT);
                                mensajeganar.show();
                            }
                        }else{
                            Toast tos = Toast.makeText(getApplicationContext(),"Has superado el máximo de intentos",Toast.LENGTH_SHORT);
                            tos.show();
                        }
                    }
                    numero.setText("");
                    TextView intentosText = findViewById(R.id.numintentos);
                    intentosText.setText("Numero de intentos:"+intentos);
                }else{
                    Toast ganado = Toast.makeText(getApplicationContext(),"No puedes jugar, ya has ganado",Toast.LENGTH_SHORT);
                    ganado.show();
                }
            }
        });
    }
    public boolean verificar(String numero){
        if(numero.equals("2")){
            return true;
        }else{
            intentos++;
        }
        return false;
    }
}
