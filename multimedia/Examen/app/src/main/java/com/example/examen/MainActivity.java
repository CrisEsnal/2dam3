package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final String MENSAJE = "nombreusu";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button boton = findViewById(R.id.log);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText nombre = findViewById(R.id.nombre);
                EditText contra = findViewById(R.id.contra);
                if(nombre.getText().toString().equals("")||contra.getText().toString().equals("")) {
                    Toast toast = Toast.makeText(getApplicationContext(),"Error, uno de los campos esta vacio",Toast.LENGTH_SHORT);
                    toast.show();
                }else{
                    String nomb = nombre.getText().toString();
                    String con = contra.getText().toString();
                    if(nomb.equals("cris")&& con.equals("1234")){
                        enviar(nomb);
                    }else{
                        Toast toast2 = Toast.makeText(getApplicationContext(), "Las credenciales no son correctas",Toast.LENGTH_SHORT);
                        toast2.show();
                        nombre.setText("");
                        contra.setText("");
                    }
                }
            }
        });
    }
    public void enviar(String nomb){
        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra(MENSAJE,nomb);
        startActivity(intent);
    }

}
