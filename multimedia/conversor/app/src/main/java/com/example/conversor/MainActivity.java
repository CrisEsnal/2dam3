package com.example.conversor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
public static final String MENSAJE = "mensaje";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button boton = findViewById(R.id.button);
        Button ir = findViewById(R.id.button2);

/*
        Switch swit = findViewById(R.id.switch1);
        swit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    LinearLayout bg = findViewById(R.id.LLa);
                    bg.setBackgroundColor(Color.rgb(31, 31, 31));
                    EditText ETa = findViewById(R.id.editText);
                    EditText ETb = findViewById(R.id.editText2);
                    ETa.setTextColor(Color.WHITE);
                    ETb.setTextColor(Color.WHITE);
                }else{
                    LinearLayout bg = findViewById(R.id.LLa);
                    bg.setBackgroundColor(Color.WHITE);
                }
            }
        });
*/
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText nudo = (EditText) findViewById(R.id.editText);
                EditText km = (EditText) findViewById(R.id.editText2);
                if(!(nudo.getText().toString().isEmpty())){
                    Float nudos = Float.parseFloat(nudo.getText().toString());
                    String resKm = Float.toString((float)(nudos * 1.852));
                    TextView kmText = (TextView)findViewById(R.id.editText2);
                    kmText.setText(resKm);
                }

                if(!(km.getText().toString().isEmpty())) {
                    Float kms = Float.parseFloat(km.getText().toString());
                    String resNudos = Float.toString((float) (kms / 1.852));
                    TextView nudosText = (TextView) findViewById(R.id.editText);
                    nudosText.setText(resNudos);
                }


            }
        });



    }
    public void enviar(View view){
        Intent intent = new Intent(this, Main2Activity.class);
        EditText ET = findViewById(R.id.editText7);
        String message = ET.getText().toString();
        intent.putExtra(MENSAJE,message);
        startActivity(intent);
    }
}
