import React,{Component} from 'react';
import {Button} from 'react-native';
class primera extends Component {
  static navigationOptions = {
    title: 'Holas',
  };
  render() {
    const {navigate} = this.props.navigation;
    return (
      <Button
        title="Ir a pagina 2"
        onPress={() => navigate('Dos', {name: 'Segunda pagina'})}
      />
    );
  }
}
export default primera;