import React,{Component} from 'react';
import {Button} from 'react-native';
class segunda extends Component {
  static navigationOptions = {
    title: 'Holas',
  };
  render() {
    const {navigate} = this.props.navigation;
    return (
      <Button
        title="Ir a pagina 1"
        onPress={() => navigate('Uno', {name: 'Primera pagina'})}
      />
    );
  }
}
export default segunda;