import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import primera from './actividades/primera.js';
import segunda from './actividades/segunda.js';
const MainNavigator = createStackNavigator({
  Uno: {screen: primera},
  Dos: {screen: segunda}
});

const App = createAppContainer(MainNavigator);

export default App;