package figuras;

public abstract class FiguraGeometrica {
	private double area,perimetro,x,y;

	public FiguraGeometrica(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public abstract double calcularArea();
	public abstract double calcularPerimetro();
	public void setArea(int area) {
		this.area = area;
	}
	public double getPerimetro() {
		return perimetro;
	}
	public void setPerimetro(int perimetro) {
		this.perimetro = perimetro;
	}
	
}
