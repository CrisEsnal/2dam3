package figuras;

import java.util.*;

public class Main {
	public static void main(String[]args) {
		ArrayList<FiguraGeometrica> lista = new ArrayList<FiguraGeometrica>();
		Scanner scan = new Scanner(System.in);
		int opcion =-1;
		while(opcion!=0) {
		try {
			System.out.println("-----------------------------------------");
			System.out.println("| �Que quieres hacer?                   |\n"
					+ "| 1.- Introducir Circulo                |\n"
					+ "| 2.- Introducir Rectangulo             |\n"
					+ "| 3.- Introducir Triangulo              |\n"
					+ "| 4.- Visualizar Area total             |\n"
					+ "| 0.- Terminar                          |");
			System.out.println("-----------------------------------------");
		
			
			opcion = scan.nextInt();
			
				switch(opcion) {
					case 1:
							lista.add(new Circulo(scan));
							break;
					case 2:
							lista.add(new Rectangulo(scan));
							break;
					case 3:
							lista.add(new Triangulo(scan));
							break;	
					case 4:
						double x = 0;
						double y = 0;
							for(FiguraGeometrica fig : lista) {
								x+=fig.calcularArea();
								y+=fig.calcularPerimetro();
							}
							System.out.println("el area total es:"+x);
							System.out.println("el perimetro total es:"+y);
							break;
					case 0:
						System.out.println("*********FIN*********");
						break;
				}
			}catch(Exception e){
				System.out.println("Intenta meter un numero");
			}finally {
				scan.nextLine();
			}
		}
	}
}
