package figuras;

import java.util.Scanner;

public class Circulo extends FiguraGeometrica{
	private double radio,diametro;
	
	

	public Circulo(double x, double y, double radio, double diametro) {
		super(x, y);
		this.radio = radio;
		this.diametro = diametro;
	}
	
	public Circulo(Scanner scan) {
		super(0, 0);
		boolean error = true;
		while(error) {
			
			try{
				System.out.println("Introduce el radio:");
				this.radio = scan.nextInt();	
				error = false;
			}catch(Exception e) {
				error = true;
				scan.next();
			}
		}
		
	}
	
	public double getRadio() {
		return radio;
	}
	public void setRadio(int radio) {
		this.radio = radio;
	}
	public double getDiametro() {
		return diametro;
	}
	public void setDiametro(int diametro) {
		this.diametro = diametro;
	}
	@Override
	public double calcularArea() {
		return Math.PI*Math.pow(radio,2);
	}

	@Override
	public double calcularPerimetro() {
		return 2*Math.PI*radio;
	}

}
