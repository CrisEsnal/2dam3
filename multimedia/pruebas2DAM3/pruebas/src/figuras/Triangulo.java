package figuras;

import java.util.Scanner;

public class Triangulo extends FiguraGeometrica{
	private double lado1,lado2,lado3;

	public Triangulo(double x,double y, double lado1, double lado2, double lado3) {
		super(x, y);
		this.lado1 = lado1;
		this.lado2 = lado2;
		this.lado3 = lado3;
	}
	public Triangulo(Scanner scan) {
		super(0, 0);
		boolean error = true;
		while(error) {
			try{
		System.out.println("Introduce el lado1:");
		this.lado1 = scan.nextInt();
		System.out.println("Introduce el lado2:");
		this.lado2 = scan.nextInt();
		System.out.println("Introduce el lado3:");
		this.lado3 = scan.nextInt();
		error = false;
			}catch(Exception e) {
				error = true;
				scan.next();
			}
		}
	}
	
	public void esEscaleno() {
		if((this.lado1!=this.lado2)&&(this.lado1!=this.lado3)) {
			System.out.println("El triangulo es escaleno");
		}else{
			System.out.println("El triangulo no es escaleno");
		}
	}
	
	public void esEquilatero() {
		if((this.lado1==this.lado2)&&(this.lado1==this.lado3)) {
			System.out.println("El triangulo es equilatero");
		}else {
			System.out.println("El triangulo no es equilatero");
		}
		
	}
	public void Isosceles() {
		if((lado1==lado2&&lado1!=lado3)||(lado2==lado3&&lado2!=lado1)||(lado3==lado1&&lado3!=lado2)) {
			System.out.println("El triangulo es isosceles");
		}
	}
	public double getLado1() {
		return lado1;
	}

	public void setLado1(int lado1) {
		this.lado1 = lado1;
	}

	public double getLado2() {
		return lado2;
	}

	public void setLado2(int lado2) {
		this.lado2 = lado2;
	}

	public double getLado3() {
		return lado3;
	}

	public void setLado3(int lado3) {
		this.lado3 = lado3;
	}
	@Override
	public double calcularPerimetro() {
		return lado1+lado2+lado3;
	}

	@Override
	public double calcularArea() {
		double semiper = calcularPerimetro()/2;
		return Math.sqrt(semiper*(semiper-lado1)*(semiper-lado2)*(semiper-lado3));
	}

}
