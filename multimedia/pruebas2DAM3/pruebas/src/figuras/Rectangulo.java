package figuras;

import java.util.Scanner;

public class Rectangulo extends FiguraGeometrica{
	private double base,altura;

	
	public Rectangulo(Scanner scan) {
		super(0, 0);
		boolean error = true;
		while(error) {
			try{
				System.out.println("Introduce el base:");
				this.base = scan.nextInt();
				System.out.println("Introduce el altura:");
				this.altura = scan.nextInt();
				error = false;
			}catch(Exception e) {
				error = true;
				scan.next();
			}
		}
	}
	public double getBase() {
		return base;
	}
	public void setBase(int base) {
		this.base = base;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(int altura) {
		this.altura = altura;
	}
	@Override
	public double calcularArea() {

		return base*altura;
	}
	@Override
	public double calcularPerimetro() {
		return 2*base+2*altura;
	}
}
