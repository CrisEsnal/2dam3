package pruebas;

import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
	/*
		ArrayList<Ejemplar> lista = new ArrayList<Ejemplar>();
		
		for(int i = 0;i<10;i++) {
			lista.add(new Ejemplar(i,"Titulo"+i));
		}
		System.out.println("--------Datos--------");
		for (int i = 0;i<lista.size();i++) {
			lista.get(i).imprimir();
		}
		System.out.println("--------Datos--------");
		for(Ejemplar iej : lista) {
			iej.imprimir();
		}
		//ejemplar.setId(1234);
		
		//ejemplar.imprimir();
		*/

		ArrayList<Ejemplar> lista = new ArrayList<Ejemplar>();
		lista.add(new Libro(1,"Titulo1","autor1","editorial1"));
		lista.add(new Documental(2,"titulo2","Brandok","miedo",123)); 
		for(Ejemplar ej : lista) {
			ej.imprimir();
		}
		//Cast
		System.out.println(((Libro)lista.get(0)).getAutor());
	}
}
