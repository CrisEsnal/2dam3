package pruebas;

public abstract class Ejemplar {

	private int id;
	public String titulo;
	
	public Ejemplar() {
		this.id = 0;
		this.titulo = "";
	}
	
	public Ejemplar( int id, String titulo){
		this.id = id;
		this.titulo = titulo;
	}
	
	public void imprimir() {
		System.out.println("El id: "+this.id);
		System.out.println("El titulo es: "+this.titulo);
	}
	
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public abstract void reproducir();
		
}
