package pruebas;

public class Documental extends Ejemplar{

	private String director;
	private String tematica;
	private int duracion;
	
	public Documental(int id,String titulo, String director, String tematica, int duracion) {
		super(id,titulo);
		this.director = director;
		this.tematica = tematica;
		this.duracion = duracion;
	}
	
	
	
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getTematica() {
		return tematica;
	}
	public void setTematica(String tematica) {
		this.tematica = tematica;
	}
	public int getDuracion() {
		return duracion;
	}
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	public void imprimir() {
		System.out.println("Documental");
		super.imprimir();
		System.out.println("El director: " + this.director);
		System.out.println("La tematica es: "+this.tematica);
		System.out.println("La duracion es: " + this.duracion);
		
	}



	@Override
	public void reproducir() {
		System.out.println("Reproduciendo"); 
	}
	
}
