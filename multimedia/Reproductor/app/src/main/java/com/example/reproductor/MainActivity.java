package com.example.reproductor;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final MediaPlayer musica = MediaPlayer.create(this,R.raw.llama);
        Button Bplay = findViewById(R.id.button);
        Button Bpause = findViewById(R.id.button2);
        Switch sw = findViewById(R.id.switch2);

        Bplay.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                musica.start();
            }
        });
        Bpause.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                musica.pause();
            }
        });

    }
}
