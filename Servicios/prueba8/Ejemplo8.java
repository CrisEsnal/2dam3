import java.io.File;

public class Ejemplo8{
    public static void main(String[] args) {
        ProcessBuilder pb = new ProcessBuilder("bash");
        
        File fBat = new File("ejemplo8.sh");
        File fOut = new File("ejemplo8.txt");
        File fErr = new File("ejemplo8.err");
        
        pb.redirectInput(fBat);
        pb.redirectOutput(fOut);
        pb.redirectError(fErr);
        pb.start();

    }
}