'use strict';

var map = L.map('mapid').setView([40.39467254512296, -3.6694335937500004],6);
/*var circle = L.circle([43.345576, -1.797273],{
  color: 'green',
  fillcolor:'green',
  fillOpacity:'0.3',
  radius: 500
}).addTo(map);*/
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
  }).addTo(map);
  map.on('click', onMapClick);
  
async function imprimirAlertas(){
  var circ;
  const response = await axios.get('http://127.0.0.1:3000/veralertas');
  console.log(response.data);
  response.data.forEach(rescate => {
    var alert = L.marker([rescate.coordx,rescate.coordy]).addTo(map).on('mouseover', function(e){
      circ = L.circle([rescate.coordx,rescate.coordy],{
        color: 'green',
        fillcolor:'green',
        fillOpacity:'0.3',
        radius: rescate.rango
      }).addTo(map);
    }).on('mouseout',function(){
      if(circ!=undefined)
      map.removeLayer(circ);
    }).on('click',function(e){
      ipcRenderer.send('infoAlerta',rescate);
    });;
  });
}
async function imprimirAlerta(){
  ipcRenderer.on('chaterino',async function(event,args){

    document.getElementById('alertaid').value = args;
    const response = await axios.post('http://127.0.0.1:3000/veralerta',
      {
        id:args
      }
    );
    const rescate = response.data.doc;
      var alert = L.circle([rescate.coordx,rescate.coordy],{
        color: 'green',
        fillcolor:'green',
        fillOpacity:'0.3',
        radius: rescate.rango
      }).addTo(map);
    });

}

