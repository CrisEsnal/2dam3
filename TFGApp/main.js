// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron')
const {ipcMain} = require('electron')
const {remote} = require('electron')
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let win;
let easterwin;
var server = require('http').Server(app);
// var io = require('socket.io')(server);
// const electron = require('electron')

// Enable live reload for Electron too
require('electron-reload')(__dirname, {
    // Note that the path to electron may vary according to the main file
    electron: require(`${__dirname}/node_modules/electron`)
});
// server.listen(4848, function() {
// 	console.log('Servidor corriendo en http://localhost:4848');
// });

// io.on('connection', function(socket) {
// 	console.log('Un cliente se ha conectado');
//     socket.emit('messages', messages);
// });
function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 900,
    height: 800,
    titleBarStyle: 'hiddenInset',
    webPreferences: {
      nodeIntegration: true
    }
  })
  mainWindow.setMenuBarVisibility(false)
  mainWindow.loadFile('index.html')

  //mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}
ipcMain.on('nuevaAlertaClick',(event,coords)=>{
  //mainWindow.loadFile('crearAlerta.html');
  win = new BrowserWindow({ 
    width: 300,
    height: 450,
    parent: mainWindow,
    modal:true,
    resizable: false,
    minimizable: false,
    webPreferences: {
      nodeIntegration: true
  }})
  win.loadFile('crearAlerta.html');
  win.setMenuBarVisibility(false)
  win.show();
  win.webContents.on('did-finish-load', (args)=>{
  win.webContents.send('clickMapa',coords);
    
    //args.sender['_events']['did-finish-load']=args.sender['_events']['did-finish-load'][args.sender['_events']['did-finish-load'].length]


    //event.sender.send('clickMapa',coords);
  })
  
});
ipcMain.on('navegar',(event,arg)=>{
  mainWindow.loadFile(arg);
})
ipcMain.on('finalizarAlerta',(event,arg)=>{
  win.close();
});
ipcMain.on('finalizarModAlerta',(event,arg)=>{
  alertaWin.close();
});
ipcMain.on('easter',(event,arg)=>{
  setInterval(() => {
    easterwin = new BrowserWindow({
      width: 410,
      height: 200,
      resizable: false,
      minimizable: true,
      webPreferences: {
        nodeIntegration: true
    }
    })
    x = Math.floor(Math.random()*1920);
    y = Math.floor(Math.random()*1080);
    easterwin.setPosition(x, y)
    easterwin.loadFile('easter.html');
    easterwin.setMenuBarVisibility(false)
    easterwin.show();
  }, 1000);
});
ipcMain.on('infoAlerta',(event,alerta)=>{
  alertaWin = new BrowserWindow({
    width: 500,
    height: 500,
    resizable: false,
    minimizable: true,
    parent: mainWindow,
    modal:true,
    webPreferences: {
      nodeIntegration: true
  }
  });
  alertaWin.setMenuBarVisibility(false);
  alertaWin.loadFile('infoAlerta.html');
  alertaWin.webContents.on('did-finish-load',(args)=>{
    alertaWin.webContents.send('alertainfo',alerta);
  })
});
ipcMain.on('recargar',(evet,args)=>{
  mainWindow.reload();
})
ipcMain.on('recargar2',(evet,args)=>{
  alertaconchat.reload();
})
ipcMain.on('abrirchat',(event,alertaid)=>{
  alertaconchat = new BrowserWindow({
    width: 500,
    height: 1000,
    resizable: false,
    minimizable: true,
    parent: alertaWin,
    modal:true,
    webPreferences: {
      nodeIntegration: true
  }
  });
  alertaconchat.setMenuBarVisibility(false);
  alertaconchat.loadFile('chat.html');
  alertaconchat.webContents.on('did-finish-load',(args)=>{
    alertaconchat.webContents.send('chaterino',alertaid);
  });
});
ipcMain.on('abrirlistausus',(event,idalerta)=>{
  listausus = new BrowserWindow({
    width: 500,
    height: 500,
    resizable: false,
    minimizable: true,
    parent: alertaWin,
    modal:true,
    webPreferences: {
      nodeIntegration: true
  }
  });
  listausus.setMenuBarVisibility(false);
  listausus.loadFile('listausus.html');
  listausus.webContents.on('did-finish-load',(args)=>{
    listausus.webContents.send('listerino',idalerta);
  });
})
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
