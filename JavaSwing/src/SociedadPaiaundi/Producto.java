package SociedadPaiaundi;

public class Producto {
private int id;
private String nombre;
private String tipo;
private double precio;
private int cant;
public Producto(int id,String nombre, String tipo, double precio) {
this.nombre = nombre;
this.tipo = tipo;
this.precio = precio;
this.cant = 0;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getCant() {
	return cant;
}
public void setCant(int cant) {
	this.cant = cant;
}
public Producto() {
super();
}
public String getNombre() {
return nombre;
}
public void setNombre(String nombre) {
this.nombre = nombre;
}
public String getTipo() {
return tipo;
}
public void setTipo(String tipo) {
this.tipo = tipo;
}
public double getPrecio() {
return precio;
}
public void setPrecio(double precio) {
this.precio = precio;
}

}
