package SociedadPaiaundi;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class AccesoDatos {
	static String url2 = "jdbc:mysql://localhost/vinoteca";
	public static Connection abrirCon() {
		Connection conexion = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		try {
			conexion = DriverManager.getConnection(url2,"root","root");
			conexion.setAutoCommit(false);
		}catch(SQLException e){
			e.printStackTrace();
		}
		return conexion;
	}
	public boolean entrar(String cod,String con) {
		Connection conn = abrirCon();
		boolean entrar = false;
		String sql = "SELECT * FROM usuario where nombreusu = '"+cod+"' and contrasena = '"+con+"';";
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			if(rs.next()) {
				entrar = true;
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return entrar;
	}
	public ArrayList<Producto> listaProductos(){
		ArrayList<Producto> list = new ArrayList<Producto>();
		int a = 0;
		String b = null;
		String c = null;
		Double d = null;
		String sql = "SELECT id,nombre,tipo,precio,stock FROM producto;";
		Connection conn = abrirCon();
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			while(rs.next()) {
					Producto p = new Producto();
					
					p.setId(rs.getInt("producto.id"));
					p.setNombre(rs.getString("producto.nombre"));
					p.setTipo(rs.getString("producto.tipo"));
					p.setPrecio(rs.getDouble("producto.precio"));
					p.setCant(rs.getInt("producto.stock"));
					/*System.out.println(a);
					System.out.println(b);
					System.out.println(c);
					System.out.println(d);*/
				
				list.add(p);
			}conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	
		return list;
	}
	public void anadirAlCarro(Producto p) {
		String sql = "INSERT INTO carro (idProd,cantidad) values ("+p.getId()+","+p.getCant()+")";
		try {
			Connection conn = abrirCon();
			Statement st = conn.createStatement();
			st.executeUpdate(sql);
			conn.commit();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void asignarCarro(String cod) {
		String sql = "SELECT id FROM carro order by id desc; ";
		Connection conn = null;
		try {
			conn = abrirCon();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			rs.next();
			int d = rs.getInt("id");
			String sql1 = "INSERT INTO factura (idUsuario,idCarro) values("+cod+","+d+")";
			st.execute(sql1);
			conn.commit();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}