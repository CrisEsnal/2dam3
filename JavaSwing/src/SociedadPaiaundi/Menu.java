package SociedadPaiaundi;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.table.DefaultTableModel;


import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JTable;

public class Menu {
	static AccesoDatos ad = new AccesoDatos();
	private JFrame frame;
	private DefaultTableModel mod;
	private DefaultTableModel moda;
	private JTable table;
	private ArrayList<Producto> factura = new ArrayList<Producto>();
	public static ArrayList<Producto> lista;
	private String codi;
	private JTable table_1;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Menu() {
		initialize();
	}
	public Menu(String cod) {
		initialize();
		codi = cod;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		lista = ad.listaProductos();
		String[] columns = { "Articulo", "Cantidad", "Precio" };
		mod = new DefaultTableModel(null, columns);

		table = new JTable();
		table.setEnabled(false);
		table.setModel(mod);
		mod.setColumnIdentifiers(columns);
		frame = new JFrame();
		frame.setBounds(100, 100, 900, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//ImageIcon coca = new ImageIcon("imagenes/cocacola.png");
		//Image imagen = coca.getImage();
		//imagen = imagen.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
		//coca.setImage(imagen);
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);

		JPanel Vino = new JPanel();
		tabbedPane.addTab("Vino", null, Vino, null);

		JPanel Tabaco = new JPanel();
		tabbedPane.addTab("Tabaco", null, Tabaco, null);

		JPanel Caf� = new JPanel();
		tabbedPane.addTab("Caf\u00E9", null, Caf�, null);

		JPanel Refrescos = new JPanel();
		tabbedPane.addTab("Refrescos", null, Refrescos, null);

		JPanel Licores = new JPanel();
		tabbedPane.addTab("Licores", null, Licores, null);

		JPanel Conservas = new JPanel();
		tabbedPane.addTab("Conservas", null, Conservas, null);

		JPanel Cocina = new JPanel();
		tabbedPane.addTab("Cocina", null, Cocina, null);

		JPanel Helados = new JPanel();
		tabbedPane.addTab("Helados", null, Helados, null);
		mod.addRow(columns);

		for (Producto p : lista) {
			JButton b = new JButton(p.getNombre());

			switch (p.getTipo()) {
			case "Vino":
				Vino.add(b);
				break;
			case "Tabaco":
				Tabaco.add(b);
				break;
			case "Caf�":
				Caf�.add(b);
				break;
			case "Refrescos":
				Refrescos.add(b);
				break;
			case "Licores":
				Licores.add(b);
				break;
			case "Conservas":
				Conservas.add(b);
				break;
			case "Cocina":
				Cocina.add(b);
				break;
			case "Helados":
				Helados.add(b);
				break;

			default:
				break;
			}

			b.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					String i = JOptionPane.showInputDialog("Introduce la cantidad de productos que desea");
					p.setCant(Integer.parseInt(i));
					String pr[] = { i, p.getNombre(), p.getPrecio() * Integer.parseInt(i) + "" };
					mod.addRow(pr);
					factura.add(p);
				}
			});

		}
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.EAST);

		JButton btnNewButton = new JButton("Reservas");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);;
				Reservas r = new Reservas(frame);
			}
		});
		panel.add(btnNewButton);
		panel.add(table);

		JButton total = new JButton("Calcular total");
		panel.add(total);
		
		JButton almacen = new JButton("Almacen");
		almacen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] columns2 = { "Nombre", "Tipo", "Stock","Precio" };
				moda = new DefaultTableModel(null, columns2);
				table_1 = new JTable();
				table_1.setEnabled(false);
				table_1.setModel(moda);
				moda.setColumnIdentifiers(columns2);
				moda.addRow(columns2);
				table_1.setVisible(false);
				panel.add(table_1);
				ArrayList<Producto> lis = ad.listaProductos();
				int i  =0;
				//moda.addRow(new Object[]{"Nombre", "Tipo", "Stock","Precio"});
				for(Producto p : lis) {
					System.out.println(new Object[] {p.getNombre(),p.getTipo(),p.getCant(),p.getPrecio()});
					moda.addRow(new Object[] {p.getNombre(),p.getTipo(),p.getCant(),p.getPrecio()});
				}
				table_1.setVisible(true);
				try {
					
					table_1.print();
					
				} catch (PrinterException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				table_1.setVisible(false);
				
			}
		});
		
		panel.add(almacen);
		
		
		frame.setVisible(true);
		total.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Double total = 0.0;
				for (Producto prod : factura) {
					int cantidad = prod.getCant();
					Double precio = prod.getPrecio();
					ad.anadirAlCarro(prod);
					total += cantidad * precio;
					ad.asignarCarro(codi);
				}
				JOptionPane.showMessageDialog(null, "Total a pagar : " + total + "�");
			}

		});
	}

}
