package Ejercicio2;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Ventana extends JFrame{
	private JTextField apellidoPat = new JTextField(20);
	private JTextField apellidoMat = new JTextField(20);
	private JTextField nombres = new JTextField(20);
	private JComboBox cb = new JComboBox();
	private ButtonGroup grupo1 = new ButtonGroup();
	private JRadioButton hombre = new JRadioButton("Hombre", true);
	private JRadioButton mujer = new JRadioButton("Mujer", true);
	private JButton bGuardar = new JButton("Guardar");
	private JTextArea ta = new JTextArea(20,20);
	class EventoBotonPulsado implements ActionListener {
	private String res = "";
		public void actionPerformed(ActionEvent e) {
			res = res.concat(apellidoPat.getText());
			res = res.concat(",");
			res =res.concat(apellidoMat.getText());
			res =res.concat(",");
			res =res.concat(nombres.getText());
			res =res.concat(",");
			res = res.concat((String) cb.getSelectedItem());
			res = res.concat(",");
			if(hombre.isSelected()) {
				res =res.concat("Hombre");
			}else {
				res =res.concat("Mujer");
			}
			res =res.concat("\n");
			ta.setText(res);
		}
		
	}
	public Ventana() {
		super("Loro");
		
		setSize(280, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		cp.add(new JLabel("Apellido Paterno"));
		cp.add(apellidoPat);
		cp.add(new JLabel("Apellido Materno"));
		cp.add(apellidoMat);
		cp.add(new JLabel("Nombres"));
		cp.add(nombres);
		cb.addItem("Soltero/a");
		cb.addItem("Casado/a");
		cb.addItem("Viudo/a");
		cb.addItem("Divorciado/a");
		cp.add(cb);
		grupo1.add(hombre);
		grupo1.add(mujer);
		cp.add(hombre);
		cp.add(mujer);
		cp.add(bGuardar);
		bGuardar.addActionListener(new EventoBotonPulsado());
		cp.add(ta);
	}
}
