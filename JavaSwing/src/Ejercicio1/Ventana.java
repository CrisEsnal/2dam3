package Ejercicio1;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;



public class Ventana extends JFrame{
	private JTextField texto;
	class EventoBotonPulsado implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			texto.setText("Hola");
		}
		
	}
	class EventoBotonPulsado2 implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			texto.setText("Adios");
		}
		
	}
	public Ventana() {
		super("Loro");

		setSize(400, 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		 texto = new JTextField(20);
		JButton boton = new JButton("Di hola");
		JButton boton2 = new JButton("Di adios");
		boton.addActionListener(new EventoBotonPulsado());
		boton2.addActionListener(new EventoBotonPulsado2());
		cp.add(boton);
		cp.add(boton2);
		
		cp.add(texto);
				
	}
}
