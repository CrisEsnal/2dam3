package Exponentes;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Ventana extends JFrame{
	private JTextField base = new JTextField(5);
	private JTextField exponente = new JTextField(5);
	private JLabel res = new JLabel();
	private JButton bCalculo = new JButton();
	class Calculo implements ActionListener{
		private String res = "";
		public void actionPerformed(ActionEvent e) {
			
		}
	}
	
	
public Ventana() {
		super("Loro");
		setSize(250,150);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container c = getContentPane();
		c.setLayout(new FlowLayout());
		GridLayout l  = new GridLayout(3,2);
		l.setVgap(10);
		l.setHgap(10);
		JPanel panel = new JPanel();
		panel.setLayout(l);
		
		GridLayout k = new GridLayout(1,3);
		k.setHgap(5);
		JPanel panel2 = new JPanel();
		panel2.setLayout(k);
		
		panel.add(new JLabel("Base"));
		panel.add(base);
		panel.add(new JLabel("Exponente"));
		panel.add(exponente);
		panel.add(new JLabel("Resultado"));
		panel.add(res);
		c.add(panel);
	}
}
