import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Empleado;
import model.Proyecto;

public class Main {
	static EntityManager entityManager;
	static Query query;
	public static EntityManager crearManager() {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("JPA3");
		entityManager = emfactory.createEntityManager();
		return entityManager;
	}

	public static void main(String[] args) {
		entityManager = crearManager();
		entityManager.getTransaction().begin();
		Empleado emp = entityManager.find(Empleado.class, 12);
		Proyecto pro = entityManager.find(Proyecto.class, 2);
		pro.setEmpleado(emp);
		entityManager.getTransaction().commit();
	}
}
