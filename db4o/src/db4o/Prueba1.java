package db4o;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;


public class Prueba1 {
	final static String DBPer="C:/Users/2DAM3/Desktop/pp/DBPersonas.yap";
	
	public static void main(String[] args) {
		ObjectContainer db = Db4o.openFile(DBPer);
		Persona p1 = new Persona("Gorka","Irun");
		Persona p2 = new Persona ("Miren", "Ordizia");
		Persona p3 = new Persona ("Izaskun", "Donostia");
		
		db.store(p1);
		db.store(p2);
		db.store(p3);

		db.close();

	}
}
