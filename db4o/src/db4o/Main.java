package db4o;

import java.util.Scanner;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;


public class Main {
	final static String DBPer="C:/2dam3/db4o/pruebas/DBPersonas.yap";
	public static void main(String[] args) {
		int menu = 1;

		Scanner sc = new Scanner(System.in);

		while(menu != 0) {
			System.out.println("-----------------------------------------");
			System.out.println("| �Que quieres hacer?                   |\n"
					+ "| 1.-Generar base de datos|\n"
					+ "| 2.-Leer base de datos|\n"
					+ "| 0.- Terminar|");
			System.out.println("-----------------------------------------");
			menu = sc.nextInt();
			switch(menu){
			case 1:
				ObjectContainer db = Db4o.openFile(DBPer);
				
				Empleado p1 = new Empleado("Jon Canpandegi","f1","d1");
				Empleado p2 = new Empleado ("Ivan Branson", "f2","d2");
				Empleado p3 = new Empleado ("Crish", "f3","d2");
				
				db.store(p1);
				db.store(p2);
				db.store(p3);

				db.close();
				
				break;
			case 2:
				ObjectContainer db1 = Db4o.openFile(DBPer);
				Empleado per = new Empleado();
				ObjectSet result = db1.queryByExample(per);
				if(result.size() == 0)
					System.out.println("No existen resgistros de personas en la base de datos");
				else {
				
					while(result.hasNext()) {
						Empleado emp = (Empleado) result.next();
						System.out.println("Nombre: " + emp.getNombre() +" ID: "+ emp.getId() +" IDDEP: "+ emp.getIdDep());
					}
				}
				db1.close();
				break;
			}
		}
		
	}
}
