package db4o;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

public class Prueba2 {
	final static String DBPer="C:/Users/2DAM3/Desktop/pp/DBPersonas.yap";
	
	public static void main(String[] args) {
		ObjectContainer db = Db4o.openFile(DBPer);
		Persona per = new Persona();
		ObjectSet result = db.queryByExample(per);
		if(result.size() == 0)
			System.out.println("No existen resgistros de personas en la base de datos");
		else {
		
			while(result.hasNext()) {
				Persona p = (Persona) result.next();
				System.out.println("Nombre: " + p.getNombre() + ", ciudad:" + p.getLoc());
			}
		}
		db.close();
		} 
}
