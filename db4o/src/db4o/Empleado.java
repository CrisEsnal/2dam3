package db4o;

public class Empleado {
	private String nombre;
	private String id;
	private String idDep;
	
	public Empleado(String nombre, String id, String idDep) {
		super();
		this.nombre = nombre;
		this.id = id;
		this.idDep = idDep;
	}
	
	
	public Empleado() {
		this.nombre = "";
		this.id = "";
		this.idDep = "";
	}


	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdDep() {
		return idDep;
	}
	public void setIdDep(String idDep) {
		this.idDep = idDep;
	}
	
	
		
}
