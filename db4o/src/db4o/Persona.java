package db4o;

public class Persona {
	private  String nombre;
	private  String loc;
	public Persona(String nombre, String loc) {
		this.nombre = nombre;
		this.loc = loc;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getLoc() {
		return loc;
	}
	public void setLoc(String loc) {
		this.loc = loc;
	}
	public Persona() {
		this.loc = "";
		this.nombre = "";
	}
	
}
