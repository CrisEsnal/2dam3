package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the contacto database table.
 * 
 */
@Entity
@NamedQuery(name="Contacto.findAll", query="SELECT c FROM Contacto c")
public class Contacto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String nombre;

	//bi-directional many-to-many association to Grupo
	@ManyToMany
	@JoinTable(
		name="contacto_grupo"
		, joinColumns={
			@JoinColumn(name="idContacto")
			}
		, inverseJoinColumns={
			@JoinColumn(name="idGrupo")
			}
		)
	private List<Grupo> grupos;

	//bi-directional many-to-one association to Telefono
	@OneToMany(mappedBy="contacto",cascade= {CascadeType.REMOVE})
	private List<Telefono> telefonos;

	public Contacto() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Grupo> getGrupos() {
		return this.grupos;
	}

	public void setGrupos(List<Grupo> grupos) {
		this.grupos = grupos;
	}
	public void setGrupo(Grupo grupo) {
		this.grupos.add(grupo);
	}

	public List<Telefono> getTelefonos() {
		return this.telefonos;
	}

	public void setTelefonos(List<Telefono> telefonos) {
		this.telefonos = telefonos;
	}

	public Telefono addTelefono(Telefono telefono) {
		getTelefonos().add(telefono);
		telefono.setContacto(this);

		return telefono;
	}

	public Telefono removeTelefono(Telefono telefono) {
		getTelefonos().remove(telefono);
		telefono.setContacto(null);

		return telefono;
	}

}