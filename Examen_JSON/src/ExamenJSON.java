import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ExamenJSON {

	public static void main(String[] args) throws IOException, ParseException {
		String url = "empleados.json";
		JSONArray listaEmpleados = null;
		String nombre, localidad;
		int edad, salario;
		
		System.out.println("---MENU---");
		System.out.println("1- Alta empleado");
		System.out.println("2- Subida salarial");
		System.out.println("3- Baja empleado");
		System.out.print("Opcion escogida:");
		
		Scanner sc = new Scanner(System.in);
		int opcion = sc.nextInt();
		
		switch (opcion) {
		case 1:
			listaEmpleados = leerJSON(url);
			// Si esta vacio lanza este mensaje
			if(listaEmpleados.isEmpty()) {System.out.println("El fichero esta vacio");}
			else {
				// Pedimos introducir todos los datos
				Scanner consola = new Scanner(System.in);
				System.out.println("Introduce el nombre:");
				nombre = consola.nextLine();
				System.out.println("Introduce la edad:");
				edad = Integer.parseInt(consola.nextLine());
				System.out.println("Introduce la localidad:");
				localidad = consola.nextLine();
				System.out.println("Introduce el salario:");
				salario = Integer.parseInt(consola.nextLine());
				// Creamos el objeto json con los datos introducidos
				JSONObject nuevoEmpleado = new JSONObject();
				nuevoEmpleado.put("nombre", nombre);
				nuevoEmpleado.put("edad", edad);
				nuevoEmpleado.put("localidad", localidad);
				nuevoEmpleado.put("salario", salario);
				altaEmpleados(url, nuevoEmpleado);
				System.out.println("Usuario dado de alta");
				}
			break;
		case 2:
			leerJSON(url);
			subidaSalarial(url);
			break;
			
		case 3:
			leerJSON(url);
			bajaEmpleados(url);
			break;

		default:
			System.out.println("Opcion no valida");
			break;
		}
		
	}
	
	// Metodo que leera el JSON
	public static JSONArray leerJSON(String url) throws IOException, ParseException {
		Object obj = null;
		try {
			obj = new JSONParser().parse(new FileReader(url));
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado este fichero");
			e.printStackTrace();
		}
		return (JSONArray) obj;
	}
	
	public static void altaEmpleados(String url, JSONObject nuevoEmpleado) throws IOException, ParseException {
		// Creamos el array de empleados
		JSONArray listaEmpleados = leerJSON(url);
		int id = 0;
		if(listaEmpleados.isEmpty()) {System.out.println("No hay empleados en el archivo");}
		// Con esto sumariamos al ultimo id
		else {
			 JSONObject ultimoUsuario = (JSONObject) listaEmpleados.get(listaEmpleados.size()-1);
			 id = Integer.parseInt(ultimoUsuario.get("id").toString()) + 1;
		}
		nuevoEmpleado.put("id", id);
		// Insertamos el empleado en JSONArray
		listaEmpleados.add(nuevoEmpleado);
		escrituraJSON(url, listaEmpleados);
	}
	
	// Metodo que uso para escribir el JSON
	private static void escrituraJSON(String url, JSONArray listaEmpleados) {
		try {
			PrintWriter escritor = new PrintWriter(url);
			escritor.write(listaEmpleados.toJSONString());
			escritor.flush();
			escritor.close();
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado este archivo");
			e.printStackTrace();
		}
		
	}

	public static void subidaSalarial(String url) throws IOException, ParseException {
		JSONArray listaEmpleados = leerJSON(url);
		if(listaEmpleados.isEmpty()) {System.out.println("No hay empleados en el archivo");}
		else {
			System.out.println("Nombres de los empleados:");
			for (Object emp : listaEmpleados) {
				JSONObject empleado = (JSONObject) emp;
				// Si el salario es menor que 1500 incrementamos un 5%
				/*if(empleado.get("salario") < 1500) {
					empleado.put("salario", salario * 1.05);
				}*/
				// Imprimimos todos los datos
				System.out.println("NOMBRE:");
				System.out.println(empleado.get("nombre"));
				System.out.println("EDAD:");
				System.out.println(empleado.get("edad"));
				System.out.println("LOCALIDAD:");
				System.out.println(empleado.get("localidad"));
				System.out.println("SALARIO:");
				System.out.println(empleado.get("salario"));
			}
		}
	}
	
	public static void bajaEmpleados(String url) throws IOException, ParseException {
		JSONArray listaEmpleados = leerJSON(url);
		if(listaEmpleados.isEmpty()){System.out.println("No hay empleados en el archivo");}
		else {
			for (Object emp : listaEmpleados) {
				JSONObject empleado = (JSONObject) emp;
				// Si la edad de los empleados es igual o mayor a 65, los damos de baja (los borramos)
				
				/* if (empleado.get("edad") >= 65) {
					listaEmpleados.remove(empleado);				
					 break;
				} */
			}
		}
	}

}
