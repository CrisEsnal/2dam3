import java.util.Iterator;

import org.bson.Document;

import com.mongodb.client.FindIterable;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class Pruebas {
	public static void main(String[] args) {

		MongoClient mongoClient = new MongoClient("localhost",27017);
		MongoDatabase db = mongoClient.getDatabase("mibasedatos");

		FindIterable findIterable = db.getCollection("empleado").find();
		Iterator iter = findIterable.iterator();
		while (iter.hasNext()) {
		Document doc = (Document) iter.next();
		System.out.println(doc.toJson());
		}
		mongoClient.close();
		
	}
}
