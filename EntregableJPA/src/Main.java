import model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class Main {
	static Scanner sc = new Scanner(System.in);
	static EntityManager entityManager;
	static Query query;
	static EntityManager em = crearManager();
	public static void escribirMenu() {
		System.out.println("Introduce una opcion");
		//menu principal
		System.out.println("/*******************Menu*******************\\");
		System.out.println("| 1.- A�adir                               |");
		System.out.println("| 2.- Consultar                            |");
		System.out.println("| 3.- Modificar                            |");
		System.out.println("| 4.- Asociar contacto                     |");
		System.out.println("| 5.- Eliminar contacto                    |");
		System.out.println("| 6.- Eliminar Grupo                       |");
		System.out.println("| 7.- Salir                                |");
		System.out.println("\\******************************************/");
	}
	public static void escribirMenuAnadir() {
		//menu para a�adir
		System.out.println("/*******************Menu*******************\\");
		System.out.println("| 1.- A�adir Contacto                      |");
		System.out.println("| 2.- A�adir Tel�fono                      |");
		System.out.println("| 3.- A�adir Grupo                         |");
		System.out.println("\\******************************************/");
	}
	public static void escribirMenuConsultar() {
		System.out.println("/**********************************Menu***********************************\\");
		System.out.println("| 1.- Consultar datos y los tel�fonos de un contacto                      |");
		System.out.println("| 2.- Consultar contactos de un grupo determinado junto con sus tel�fonos |");
		System.out.println("| 3.- Consultar grupos existentes junto con el n� de contactos asociados  |");
		System.out.println("\\*************************************************************************/");
	}
	public static EntityManager crearManager() {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("EntregableJPA");
		entityManager = emfactory.createEntityManager();
		return entityManager;
	}
	public static Query consultar(String q) {		
		query  = entityManager.createQuery(q);
		return query;
	}
	public static void crearContacto() {
		//Introduce el nombre del contacto y se empieza a crear el contacto
		System.out.println("Introduce un nombre");
		Contacto c = new Contacto();
		c.setNombre(sc.nextLine());
		em.getTransaction().begin();
		em.persist(c);
		em.getTransaction().commit();
		System.out.println("Contacto creado");
	}
	public static void crearTelefono() {
		//creacion de telefono
		System.out.println("Introduce el numero de telefono");
		Telefono t = new Telefono();
		t.setNumero(sc.nextLine());
		query = entityManager.createNamedQuery("Contacto.findAll");
		List<Contacto> cont = query.getResultList();
		for(Contacto con : cont) {
			System.out.println("ID: "+con.getId()+" Nombre: " + con.getNombre());
		}
		System.out.println("Introduce el id del contacto");
		int a = sc.nextInt();
		Contacto contacti = em.find(Contacto.class,a);
		t.setContacto(contacti);
		sc.nextLine();
		em.getTransaction().begin();
		em.refresh(contacti);
		em.persist(t);
		em.getTransaction().commit();
	}
	public static void crearGrupo() {
		System.out.println("Introduce el nombre del grupo");
		String b = sc.nextLine();
		Grupo gru = new Grupo();
		gru.setNombre(b);
		try {
			em.getTransaction().begin();
			em.persist(gru);
			em.getTransaction().commit();
			System.out.println("Grupo creado");
		}catch (Exception e) {
			System.out.println("Error al crear el grupo");
		}
	}
	public static void visualizarContactos() {
		query = em.createNamedQuery("Contacto.findAll");
		List<Contacto> cont = query.getResultList();
		for(Contacto c : cont) {
			System.out.println("ID: "+c.getId()+" Nombre: " + c.getNombre());
		}
	}
	public static void buscarContactoyTelefonos(int id) {
		query = consultar("SELECT c FROM Contacto c  where c.id ="+id);
		Contacto con = (Contacto) query.getSingleResult();
		System.out.println("__________Contacto__________");
		System.out.println(con.getNombre());
		List<Telefono> tel = con.getTelefonos();
		System.out.println("__________Telefonos_________");
		for(Telefono t:tel) {
			System.out.println(t.getNumero());
		}
		System.out.println("____________________________");
	}
	public static void listaGrupos() {
		query = entityManager.createNamedQuery("Grupo.findAll");
		List<Grupo> g = query.getResultList();
		System.out.println("__________Grupos__________");
		for(Grupo gru:g) {
			System.out.println("Id: "+gru.getId()+"Nombre: "+gru.getNombre());
		}
		System.out.println("____________________________");
	}
	public static void infoContactosdeunGrupo(int idGrupo) {
		//genero una lista de contactos a partir de la busqueda de id de grupo
		Grupo g = (Grupo) em.find(Grupo.class, idGrupo);
		List<Contacto> con = g.getContactos();
		for(Contacto c :con) {
			buscarContactoyTelefonos(c.getId());
		}
	}
	public static void gruposycantidad() {
		//busco todos los contactos y los meto en una lista
		query = entityManager.createNamedQuery("Grupo.findAll");
		List<Grupo> g = query.getResultList();
		System.out.println("__________Grupos__________");
		for(Grupo gru:g) {
			//recorro todos los contactos de la lista imprimiendo los datos
			System.out.println("Id: "+gru.getId()+" | Nombre: "+gru.getNombre()+" | Cantidad de integrantes: "+gru.getContactos().size());
		}
		System.out.println("____________________________");
	}
	public static void modContacto(int id,String nombre) {
		//busco el contacto por el id
		Contacto con = entityManager.find(Contacto.class,id);
		try{
			//cambio el nombre del contacto buscado por el introducidos
			em.getTransaction().begin();
			con.setNombre(nombre);
			em.getTransaction().commit();
		}catch(Exception e) {
			System.out.println("No se ha podido modificar");
		}
		
	}
	public static void asociargrupo(int idCon, ArrayList<Grupo> grupos) {
		//busco un contacto y le meto el arraylist con grupos
		Contacto con = (Contacto) em.find(Contacto.class, idCon);
		em.getTransaction().begin();
		con.setGrupos(grupos);
		//em.refresh(con);
		em.getTransaction().commit();
	}
	public static void eliminar(int idCon) {
		Contacto con = (Contacto) em.find(Contacto.class, idCon);
		em.getTransaction().begin();
		em.remove(con);
		em.getTransaction().commit();
	}
	public static void eliminarGrupo(int idgru) {
		Grupo gru = (Grupo) em.find(Grupo.class, idgru);
		em.getTransaction().begin();
		em.remove(gru);
		em.getTransaction().commit();
	}
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		int menu,menu2;
		do {
			escribirMenu();
			menu = sc.nextInt();
			switch(menu) {
			case 1:
				escribirMenuAnadir();
				menu2 = sc.nextInt();
				switch(menu2) {
					case 1:
						sc.nextLine();
						crearContacto();
						break;
					case 2:
						sc.nextLine();
						crearTelefono();
						break;
					case 3:
						sc.nextLine();
						crearGrupo();
				}
				break;
			case 2:
				escribirMenuConsultar();
				menu2 = sc.nextInt();
				switch(menu2) {
					case 1:
						visualizarContactos();
						System.out.println("Introduce un id de contacto");
						int a = sc.nextInt();
						buscarContactoyTelefonos(a);
						break;
					case 2:
						listaGrupos();
						System.out.println("Introduce un id de grupo");
						int idGrupo = sc.nextInt();
						infoContactosdeunGrupo(idGrupo);
						break;
					case 3:
						gruposycantidad();
						break;						
				}
				break;
			case 3:
				sc.nextLine();//se visualizan los contactos y se pide un id de contacto
				visualizarContactos();
				System.out.println("Introduce el id de contacto");
				int idcon = sc.nextInt();
				sc.nextLine();
				//se introduce un nombre de contacto para poder modificarlo
				System.out.println("Introduce el nuevo nombre para el contacto");
				String con = sc.nextLine();
				modContacto(idcon,con);
			case 4:
				sc.nextLine();
				int menu3 = 1;
				//Visualizo los contactos para que introduzca un id
				visualizarContactos();
				System.out.println("Introduce el id del contacto");
				int idcont = sc.nextInt();
				int idgrupo = 0;
				ArrayList<Grupo> listaGrupos = new ArrayList<Grupo>();
				listaGrupos();
				while(menu3 != 0) {
					sc.nextLine();
					//Introduccion de un id de grupo y se a�ade al arraylist buscando por el id de grupo
					System.out.println("Introduce el id del grupo al que quieres asociarlo");
					idgrupo = sc.nextInt();
					listaGrupos.add(em.find(Grupo.class,idgrupo));
					sc.nextLine();
					//pregunta para si son mas de un grupo los que se quieren asociar
					System.out.println("Desea asociar m�s grupos? si=1 no=0");
					menu3 = sc.nextInt();
				}
				asociargrupo(idcont,listaGrupos);
				break;
			case 5:
				//Visualizo los contactos y pido un id de contacto para poder buscarlo y eliminarlo
				visualizarContactos();
				System.out.println("Introduce el id del Contacto que quieres eliminar:");
				int id = sc.nextInt();
				System.out.println("Seguro que quieres eliminar este usuario? si = 1 / no = 0");
				int elim = sc.nextInt();
				if(elim == 1) {
					eliminar(id);
				}else {
					System.out.println("No se ha eliminado el Contacto");
				}
				break;
			case 6:
				listaGrupos();
				sc.nextLine();
				System.out.println("Introduce el id del grupo que quieras eliminar");
				int idgrup = sc.nextInt();
				eliminarGrupo(idgrup);
				break;
			}
		}while(menu!=7);
	}
}


