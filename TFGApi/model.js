const mongoose = require('mongoose')

function conectar(){
mongoose.connect('mongodb://localhost/alertas', function (err) {
 
   if (err) throw err;
 
   console.log('Conectado a la base de datos');
 
});}
//ESQUEMASSSSSSSSSSSSSSSSS
const alertaSchema = new mongoose.Schema({
    nombreAlerta:{type:String,unique:true},
    descripcion:String,
    coordx:String,
    coordy:String,
    rango:String,
    text:[JSON],
    fechacreacion:Date
});
const usuSchema = new mongoose.Schema({
    usuario:{type:String,unique:true},
    contra:String,
    alertasuscritaid:String
});
const traksSchema = new mongoose.Schema({
    idusu:String,
    idalerta:String,
    traks:[JSON]
})
//FIN ESQUEMAAAAAAAAAAAAAAAAAAAAAAAS
const traks = mongoose.model('Traks',traksSchema);
//FUNCIONES ALERTAS
const alerta = mongoose.model('Alerta',alertaSchema);

function crearAlerta(req){
    conectar();
    var alerta1 = new alerta({
        nombreAlerta:req.body.nombre,
        descripcion:req.body.descripcion,
        coordx:req.body.x+"",
        coordy:req.body.y+"",
        fechacreacion:new Date(),
        rango:req.body.rango*1000+"",
        text:[]
    });
    alerta1.save();
}
function verAlertas(req,res){
        conectar();
        alerta.find({}).sort({fechacreacion:-1}).exec(function(err,json){
            res.send(json);
    });
}
function modificarAlerta(req,res){
    conectar();
    console.log(req.body)
    alerta.findOne({
        _id:req.body.idalerta
    }).exec(function(err,doc){
        doc.nombreAlerta = req.body.nombre;
        doc.descripcion = req.body.descripcion;
        doc.coordx = req.body.x;
        doc.coordy = req.body.y;
        doc.rango = req.body.rango*1000;
        doc.save();
        console.log(doc);
    });
}
function eliminarAlerta(req,res){
    conectar();
    console.log("pero el le dejo en buzon");
    console.log(req.body.id);
    alerta.findOne({_id:req.body.id}).exec(function(err,doc){
        doc.remove();
        res.send(true);
    });
    
}
function verAlerta(req,res){
    conectar();
    alerta.findOne({_id:req.body.id}).exec(function(err,doc){
        console.log(doc)
        if(doc==null){
            return res.status(500).json({
                ok:false,
                mensaje: 'Error',
                errors:err
            });
        }else{
            a = new Date() - doc.fechacreacion
            console.log(a)
            a = cambiardesegundos(a);
        }
        res.status(200).json({
            ok:true,
            doc,
            a
        });
    });
}
function cambiardesegundos(segs){
    console.log(segs);
    seconds = Math.floor((segs / 1000) % 60),
    minutes = Math.floor((segs / (1000 * 60)) % 60),
    hours = Math.floor((segs / (1000 * 60 * 60)) % 24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

  return hours + ":" + minutes + ":" + seconds;
}
function leermensajes(req,res){
    conectar();
    alerta.findOne({_id:req.body.id}).exec(function(err,doc){
        if(doc==null){
            return res.status(500).json({
                ok:false,
                mensaje: 'Error',
                errors:err
            });
        }
        res.status(200).json({
            ok:true,
            doc
        });
    });
}
// function verchat(req,res){
//     conectar();
//     alerta.findOne({_id:req.body.id}).exec(function(err,doc){
//         console.log(doc)
//         res.send(doc)
//     });
//}
function guardarMensaje(nombre,mensaje,idalerta){
    alerta.findOne({_id:idalerta}).exec(function(err,doc){
        mens = {
            nombreUsu:nombre,
            mensaje:mensaje
        };
        doc.text.push(mens);
        doc.save();
        console.log(doc,nombre,mensaje,idalerta);
    });
}
// function leerchat(req,res){
//     conectar();
//     alerta.findOne({_id:req.body.idAlerta});
// }
//FUNCIONES USUARIOS
const usuario = mongoose.model('Usuario',usuSchema);

function crearUsu(req,res){
    conectar();
    var usuario1 = new usuario({
        usuario:req.body.nombre,
        contra:req.body.contra,
        alertasuscritaid:"0"
    });
    usuario.findOne({usuario:req.body.nombre}).exec(function(err,doc){
        console.log();
        if(doc==null){
            usuario1.save();
            res.send(true);
        }else if(doc.usuario == req.body.nombre){
            return res.status(500).json({
                ok:false,
                mensaje: 'Error',
                errors:err
            });
        }
    });
}
function listausuarios(req,res){
    conectar();
    console.log("CHUPUNOZO"+req.body.id);
    usuario.find({alertasuscritaid:req.body.id}).exec((err,doc)=>{
        if(doc==null){
            console.log(doc+"CHUPINAZOOOOOOOOO")
            return res.status(500).json({
                ok:false,
                mensaje:'Error',
                error:err
            });
        }
        res.status(200).json({
            doc
            
        })

        console.log(doc)
    });
}
function buscarUsu(req,res){
    conectar();
    usuario.findOne({usuario:req.body.nombre,contra:req.body.contra}).exec((err,doc)=> {
        console.log(doc)
    if(doc==null){
        return res.status(500).json({
            ok:false,
            mensaje: 'Error',
            errors:err
        });
    }
    res.status(200).json({
        ok:true,
        doc
    })
   });
}
function buscarUsuId(req,res){
    conectar();
    usuario.findOne({_id:req.body.id}).exec((err,doc)=> {
        console.log(doc)
    if(doc==null){
        return res.status(500).json({
            ok:false,
            mensaje: 'Error',
            errors:err
        });
    }
    res.status(200).json({
        ok:true,
        doc
    })
   });
}
function suscribirseAlerta(req,res){
    conectar();
    usuario.findOne({_id:req.body.idusu}).exec((err,doc)=>{
        console.log(doc)
            doc.alertasuscritaid = req.body.idalerta;
            doc.save();
        if(doc==null){
            return res.status(500).json({
                ok:false,
                mensaje:'Error',
                errors:err
            });
        }
        res.status(200).json({
            ok:true
        });
    });
}
//TRACCCCKKKKKKKKSSSSS
function leerUltimoTrack(coord){
    conectar();
    var ultimisimo;
    traks.findOne({idusu:coord.idusu,idalerta:coord.idalerta}).exec((err,doc)=>{
        if(doc==null){
            console.log("WEIWIWEIWIEWIEWEI")
            return res.status(500).json({
                ok:false,
                mensaje:'Error',
                errors:err
            });
        }
        ultimo = doc.traks;
        ultimisimo = ultimo[ultimo.length-1].x
        console.log(ultimo[ultimo.length-1].x+"PAPAUMA"+ultimisimo+"eeeeeeeeeeeeeeeeeeeeeeeeeee");
        console.log(ultimisimo);
        return ultimisimo;
    });
    return ultimisimo;
}
function guardarTrack(coord){
    conectar();
    console.log(coord.idusu+"eeeeee"+coord.idalerta);
   
    traks.findOne({idusu:coord.idusu,idalerta:coord.idalerta}).exec((err,doc)=>{
        if(doc==null){
            if(coord.lataux!=null&&coord.longaux!=null){

            a = {x:coord.lataux,y:coord.longaux};
            var track = new traks({
                idusu : coord.idusu,
                idalerta : coord.idalerta,
                traks : a
            });
            track.save();
        }
        }else{
            
            ultimo = doc.traks;
            ultimisimo = ultimo[ultimo.length-1].x
            //console.log(ultimisimo +"dfsds"+coord.lataux)
            //if(ultimo[ultimo.length-1].x!=coord.lataux&&ultimo[ultimo.length-1].y!=coord.longaux){
                    a = {x:coord.lataux,y:coord.longaux};
                    console.log(doc+"AIUAUAUAUIAUI");
                    doc.traks.push(a);
                    doc.save();
             //}
            
        }
    })
}
function leerTrack(req,res){
    traks.find({idalerta:req.params.idalerta,idusu:req.params.idusu}).exec(function(err,doc){
        if(doc == undefined){
            return res.status(500).json({
                ok:false,
                mensaje:'Error',
                errors:err
            });
        }else{
            res.status(200).json({
                ok:true,
                doc:doc
            });
        }
    });
}
function leerTracks(req,res){
    traks.find({idalerta:req.params.idalerta}).exec(function(err,doc){
        if(doc == undefined){
            return res.status(500).json({
                ok:false,
                mensaje:'Error',
                errors:err
            });
        }else{
            res.status(200).json({
                ok:true,
                doc:doc
            })
        }
        console.log(doc)
    });
}

module.exports = {leerTrack,leerTracks,guardarTrack,leermensajes,buscarUsuId,listausuarios,conectar,crearAlerta,verAlertas,buscarUsu,crearUsu,modificarAlerta, guardarMensaje,leerUltimoTrack,eliminarAlerta,verAlerta,suscribirseAlerta};