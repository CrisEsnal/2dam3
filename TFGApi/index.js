const express = require("express");
var bodyParser = require("body-parser")
var cors = require('cors');
const modelo = require('./model')
var app = express();
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())
// CORS

app.use(cors());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.listen(3000, () => {
 console.log("El servidor está inicializado en el puerto 3000");
});
//
app.get('/', function (req, res) {
    res.send('Saludos desde express');
  });
app.post('/crearalerta', function(req,res){
    console.log(req.body.nombre)
    modelo.crearAlerta(req);
})
app.get('/veralertas',modelo.verAlertas);
app.post('/veralerta',modelo.verAlerta);
app.post('/buscarusu',modelo.buscarUsu);
app.post('/crearusu',modelo.crearUsu);
app.post('/modalerta',modelo.modificarAlerta);
app.post('/elimalerta',modelo.eliminarAlerta);
app.post('/susalerta',modelo.suscribirseAlerta);
app.post('/listausus',modelo.listausuarios);
app.post('/buscarusuid',modelo.buscarUsuId);
app.post('/leermensajes',modelo.leermensajes);
app.get('/leerTracks/:idalerta',modelo.leerTracks);
app.get('/leerTrack/:idusu/:idalerta',modelo.leerTrack);
//zona chat
http = require('http'),
  app = express(),
  server = http.createServer(app),
  io = require('socket.io').listen(server);
app.get('/', (req, res) => {
  res.send('Server chat en puerto 4000')
  console.log("AYYYYYYYYYYYY");
});
io.on('connection', (socket) => {

  console.log('usu conectado')

  socket.on('join', function (nombre) {
    console.log(nombre + " : se ha unido ");
       socket.broadcast.emit('Usuario conectado', nombre + " : se ha unido ");
  })

  socket.on('messagedetection', (senderNickname, messageContent,idalerta) => {

    modelo.guardarMensaje(senderNickname,messageContent,idalerta)
    
    let message = { "mensaje": messageContent, "nombre": senderNickname,"idGrupo": idalerta }

    // send the message to all users including the sender  using io.emit() 

    io.emit('mensaje', message)
  })
  socket.on('enviarTracko',function(coord){
    modelo.guardarTrack(coord)
    console.log('actualizar'+coord.idalerta+'')
    io.emit('actualizar'+coord.idalerta,coord)
    console.log("actualizarino")
  })
  socket.on('disconnect', function () {

    console.log('user has left ')

    socket.broadcast.emit("userdisconnect", ' user has left')

  })
  
});



server.listen(4000, () => {

  console.log('Chat corriendo en 4000')

})
