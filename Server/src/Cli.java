import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

public class Cli {
	
	public static void main(String[] args) {
		iniciar();
	}
	@SuppressWarnings("resource")
	public static void iniciar() {
		try {
			Socket s;
			s = new Socket("127.0.0.1",3333);
			BufferedReader bf = new BufferedReader(new InputStreamReader(s.getInputStream()));
			String linea = null;
			while((linea = bf.readLine())!=null) {
				System.out.println("Cliente" + linea);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
