import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Servidor extends Thread {
	public static ServerSocket server;
	public static Socket client;
	public static ArrayList<Socket> clientes = new ArrayList<Socket>();
	
	public Servidor(Socket s) {
		client = s;
	}
	
	
	public static void main(String[] args) {
		
		try {
		server = new ServerSocket(4848);
		System.out.println("Server iniciado");
		} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
		}
		
		while(true) {
		try {
			
		client = server.accept();
		
		clientes.add(client);
		
		new Servidor(client).start();
		
		} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		}
	}
	public void run() {
		
		//crearHilos();
		
		try {
		
			System.out.println("alguien se ha conectado");
			BufferedWriter bw;
			
			BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
			String linea = "";
			
			while(!linea.equalsIgnoreCase("END") ) {
			linea = br.readLine();
			System.out.println("Servidor: "+linea+"\n");
			for (Socket socket : clientes) {
				bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
				bw.write(linea+"\n");
				bw.flush();
			}
			
			
			}
			System.out.println("Terminado");
		
		} catch (IOException e) {
			e.printStackTrace();
		
		}
	
	
	}

}
