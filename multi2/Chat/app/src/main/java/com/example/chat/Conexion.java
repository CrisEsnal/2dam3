package com.example.chat;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.os.Looper;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Conexion extends AsyncTask<Void,Void,Void> {
    String mensaje;
    Socket s;
    OutputStream os;
    Handler hPrincipal;
    TextView tv;
    HiloUno uno;
    BufferedReader br;
    HiloDos dos;
    String text;
    public Conexion(){

    }
    public Conexion(Handler hPrincipal,TextView tv){
        this.hPrincipal = hPrincipal;

        this.tv = tv;
    }
    @Override
    protected Void doInBackground(Void... voids) {
        //Log.e("Conexion", "Conectando....");
        try{
        s = new Socket("10.0.2.2",4848);
        // InetSocketAddress addr = new InetSocketAddress("10.0.2.2",4848);
       // Log.e("Conexion", "Sigue Conectando....");

        // s.connect(addr);
        Log.e("Conexion", "Conectado!");
        os=s.getOutputStream();
        br = new BufferedReader(new InputStreamReader(s.getInputStream()));
            //while((text = br.readLine()) != null) {
            Log.e("mensaje",br.readLine());
           // }
            dos =  new HiloDos("dos");
            dos.start();
            Looper looperdos = dos.getLooper();
            final Handler handler = new Handler(looperdos);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    try {

                        while((mensaje = br.readLine())!=null){
                            Log.e("PEPE", "INTENTANDO RECIBIR UN MENSAJE");

                            hPrincipal.post(new Runnable() {
                                @Override
                                public void run() {
                                    tv.append("\n"+mensaje);
                                }
                            });
                            Log.e("mensajeServidor",mensaje);
                            // write(mensaje);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });


      //  s.close();
        }catch (IOException e){
            Log.e("Conexion", "ERROORRRRR....");
            e.printStackTrace();
        }
        return null;
    }

    public void write(String msg){
        mensaje = msg;
    }
    public void escribir(String msg){
       final String mess = msg;
        uno =  new HiloUno("uno");
        uno.start();
        Looper looperuno = uno.getLooper();
        final Handler handler = new Handler(looperuno);
        handler.post(new Runnable() {
            @Override
            public void run() {
                String aux = mess+"\n";

                    try {
                        os.write(aux.getBytes());
                        os.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


        });

    }
    public void desconectar(){
        try {
            dos.getLooper().quit();
            dos.quit();
            uno.getLooper().quit();
            uno.quit();
            s.close();



        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
