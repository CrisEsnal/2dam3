package com.example.rescates;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button botonLogin = findViewById(R.id.crear);
        final EditText nombre = findViewById(R.id.nombre);
        final EditText contra = findViewById(R.id.contra);
        Button botonReg = findViewById(R.id.reg);

        botonReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registro = new Intent(v.getContext(),Registro.class);
                startActivity(registro);
            }
        });
        botonLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                HacerPost(nombre.getText().toString(),contra.getText().toString());
            }
        });


        /*botonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            final
                            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                            conn.setRequestProperty("User-Agent", "my-rest-app-v0.1");


                            if(conn.getResponseCode()==200){
                                Log.d("aaa", "aaaaaaaaaaa");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });*/
    }
    public void HacerPost(final String usuario, final String contra){
        String buscarusu = "http://10.0.2.2:3000/buscarusu";
        StringRequest llamadaPost = new StringRequest(Request.Method.POST, buscarusu, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Intent menuprinc = new Intent(MainActivity.this,MenuPrincipal.class);
                menuprinc.putExtra("usu",response);
                startActivity(menuprinc);


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast t = Toast.makeText(getApplicationContext(), "El usuario y contraseña son incorrectos",Toast.LENGTH_SHORT);
                        t.show();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                params.put("nombre", usuario);
                params.put("contra", contra);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(llamadaPost);

    }

}
