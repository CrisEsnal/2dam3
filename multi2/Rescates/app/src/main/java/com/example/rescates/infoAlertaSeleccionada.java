package com.example.rescates;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class infoAlertaSeleccionada extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_alerta_seleccionada);
        String id = getIntent().getStringExtra("idbusqueda");
        String nombre = getIntent().getStringExtra("nombreusu");
        Log.d("datos",id + nombre);
        getalerta();
        Button bsus = findViewById(R.id.sus);
        bsus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suscribirse();
            }
        });
    }
    public void getalerta(){
        String infoalerta = "http://10.0.2.2:3000/veralerta";
        StringRequest llamadaget = new StringRequest(Request.Method.POST, infoalerta, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject alerta = null;
                try {
                    Log.d("jiji response",response);
                    alerta = new JSONObject(response);
                    JSONObject doc = new JSONObject(alerta.getString("doc"));
                    TextView id = findViewById(R.id.id);
                    TextView nombre = findViewById(R.id.nombre);
                    TextView desc = findViewById(R.id.descripcion);
                    TextView coordx = findViewById(R.id.corx);
                    TextView coordy = findViewById(R.id.cory);
                    TextView rad = findViewById(R.id.rad);
                    id.setText(doc.getString("_id"));
                    nombre.setText(doc.getString("nombreAlerta"));
                    desc.setText(doc.getString("descripcion"));
                    coordx.setText(doc.getString("coordx"));
                    coordy.setText(doc.getString("coordy"));
                    rad.setText(doc.getString("rango"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                String id = getIntent().getStringExtra("idbusqueda");
                Log.d("dato post", id);
                params.put("id", id);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(llamadaget);
    }
    public void suscribirse(){
        String susAlerta = "http://10.0.2.2:3000/susalerta";
        StringRequest llamadaget = new StringRequest(Request.Method.POST, susAlerta, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject alerta = null;
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                String id = getIntent().getStringExtra("idbusqueda");
                String nombre = getIntent().getStringExtra("nombreusu");
                params.put("id", id);
                try {
                    JSONObject datosusu = new JSONObject(nombre);
                    JSONObject doc = new JSONObject(datosusu.getString("doc"));
                    String idusu = doc.getString("_id");
                    params.put("idusu",idusu);

                    TextView idalertatext = findViewById(R.id.id);
                    String idalerta = idalertatext.getText().toString();
                    params.put("idalerta",idalerta);
                    finish();

                }catch (JSONException e){
                    e.printStackTrace();
                }

                return params;
            }
        };
        Volley.newRequestQueue(this).add(llamadaget);
    }

}
