package com.example.rescates;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InfoAlertaSuscrita extends AppCompatActivity {
    public static Socket socket;
    public RecyclerView myRecylerView;
    public List<Mensaje> MessageList;
    public ChatBoxAdapter chatBoxAdapter;
    public Button send;
    public EditText messagetxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final String idAlerta = getIntent().getStringExtra("alertaid");
        final String idUsuario = getIntent().getStringExtra("idUsuario");
        final String nombreUsu = getIntent().getStringExtra("nombreusu");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_alerta_suscrita);
        getalerta();
        send = findViewById(R.id.enviar);
        messagetxt = findViewById(R.id.mensaje);
        try {
            socket = IO.socket("http://10.0.2.2:4000");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        socket.connect();

        MessageList = new ArrayList<>();
        StringRequest  leermensajes  = new StringRequest(Request.Method.POST, "http://10.0.2.2:3000/leermensajes",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("AAAAAAAAAAAA",response);
                        try {
                            Log.e("AAAAAAAAAAAA",response);
                            JSONObject json = new JSONObject(response);
                            JSONObject doc = new JSONObject(json.getString("doc"));
                            JSONArray listaMensajes = new JSONArray(doc.getString("text"));
                            Log.e("iieeepaaa",listaMensajes.toString());
                            for (int i = 0; i <listaMensajes.length() ; i++) {
                                JSONObject mensaje = new JSONObject(listaMensajes.get(i).toString());
                                //JSONObject men = new JSONObject(mensaje);
                                String nombre = mensaje.getString("nombreUsu")+": ";
                                Mensaje m = new Mensaje(nombre, mensaje.getString("mensaje"),idAlerta);
                                getMensajes(m);
                                chatBoxAdapter = new ChatBoxAdapter(MessageList);

                                chatBoxAdapter.notifyDataSetChanged();
                                myRecylerView.setAdapter(chatBoxAdapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.e("getMensajes",response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error",error.toString());
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                //The POST parameters:
                params.put("id", idAlerta);
                return params;
            }
        }
                ;

        Volley.newRequestQueue(getApplicationContext()).add(leermensajes);

        myRecylerView = findViewById(R.id.messagelist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        myRecylerView.setLayoutManager(mLayoutManager);
        myRecylerView.setItemAnimator(new DefaultItemAnimator());


        // message send action
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //retrieve the nickname and the message content and fire the event messagedetection
                if(!messagetxt.getText().toString().isEmpty()) {
                    socket.emit("messagedetection", nombreUsu, messagetxt.getText().toString(),idAlerta);


                    messagetxt.setText("");
                }
            }


        });

        socket.on("mensaje", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        try {
                            //extract data from fired event

                            String nickname = data.getString("nombre");
                            String message = data.getString("mensaje");
                            String idGrupoEx = data.getString("idGrupo");

                            // make instance of message
                            if(idGrupoEx.equalsIgnoreCase(idAlerta)){
                                Mensaje m = new Mensaje(nickname, message,idAlerta);


                                //add the message to the messageList

                                MessageList.add(m);

                                // add the new updated list to the dapter
                                chatBoxAdapter = new ChatBoxAdapter(MessageList);

                                // notify the adapter to update the recycler view

                                chatBoxAdapter.notifyDataSetChanged();

                                //set the adapter for the recycler view

                                myRecylerView.setAdapter(chatBoxAdapter);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
            }
        });

        final String docki = getIntent().getStringExtra("usu");
        Log.e("aaaaaaaadssdaa",docki+"aa");
        Button alertaSusmap = findViewById(R.id.mapbutton);
        alertaSusmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("SBUHYG",getIntent().getStringExtra("usu"));
                try {
                    JSONObject doc = new JSONObject(docki);
                    Log.d("uweriuiruiretyweruity",doc.toString());
                    String idusu = doc.getString("_id");
                    crearmap(idusu);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }
    public void crearmap(final String id){
        String buscarusu = "http://10.0.2.2:3000/buscarusuid";
        StringRequest llamadaPost = new StringRequest(Request.Method.POST, buscarusu, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject doc = new JSONObject(response);
                    JSONObject usu = new JSONObject(doc.getString("doc"));
                    String alertasuscritaid = usu.getString("alertasuscritaid");
                    Log.e( "IEEEPAAAA",alertasuscritaid );

                    if(alertasuscritaid.equals("0")){
                        Toast t = Toast.makeText(getApplicationContext(), "No estas suscrito a ninguna alerta",Toast.LENGTH_SHORT);
                        t.show();
                    }else{
                        Intent maps = new Intent(InfoAlertaSuscrita.this,MapsActivity.class);
                        maps.putExtra("alertaid",alertasuscritaid);
                        maps.putExtra("idusu",usu.getString("_id"));
                        startActivity(maps);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                params.put("id", id);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(llamadaPost);

    }
    public void getMensajes(Mensaje m){
        MessageList.add(m);
    }


    public void getalerta(){
        String infoalerta = "http://10.0.2.2:3000/veralerta";
        StringRequest llamadaget = new StringRequest(Request.Method.POST, infoalerta, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject alerta = null;
                Log.e("asdsadsadsafdsdresponse",response);
                try {

                    alerta = new JSONObject(response);
                    JSONObject doc = new JSONObject(alerta.getString("doc"));
                    TextView id = findViewById(R.id.id);
                    TextView nombre = findViewById(R.id.nombre);
                    TextView desc = findViewById(R.id.descripcion);
                    //TextView coordx = findViewById(R.id.corx);
                   // TextView coordy = findViewById(R.id.cory);
                    TextView rad = findViewById(R.id.rad);
                    id.setText(doc.getString("_id"));
                    nombre.setText(doc.getString("nombreAlerta"));
                    desc.setText(doc.getString("descripcion"));
                    //coordx.setText(doc.getString("coordx"));
                    //coordy.setText(doc.getString("coordy"));
                    rad.setText(doc.getString("rango"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                String id = getIntent().getStringExtra("alertaid");

                params.put("id", id);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(llamadaget);
    }

}
