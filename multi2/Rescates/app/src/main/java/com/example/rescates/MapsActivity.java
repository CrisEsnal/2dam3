package com.example.rescates;

import androidx.fragment.app.FragmentActivity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.maps.model.PolylineOptions;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private Socket so = InfoAlertaSuscrita.socket;
    private GoogleMap mMap;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    public tracks locationTrack;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        String iddealerta = getIntent().getStringExtra("alertaid");
        getalerta();
        Log.e("hola",iddealerta);
        so.on("actualizar"+iddealerta,new Emitter.Listener() {


            @Override
            public void call(final Object... args) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // String data = (String) args[0];
                            try {
                                JSONObject track = new JSONObject(args[0].toString());
                                    Log.e("verengena",track.toString());
                                    String lat = track.getString("lat");
                                    String longi = track.getString("long");
                                    if(lat=="null"&&longi=="null"){
                                        lat = track.getString("lataux");
                                        longi = track.getString("longaux");
                                    }
                                    LatLng ant = new LatLng(Double.parseDouble(track.getString("lataux")), Double.parseDouble(track.getString("longaux")));
                                    LatLng pos = new LatLng(Double.parseDouble(lat), Double.parseDouble(longi));
                                    // Log.e("responsetrack",data);
                                    pintarTrack(pos, ant, false);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }



                            // Toast.makeText(getContext(), data, Toast.LENGTH_SHORT).show();

                        }
                    });

            }
        });

        //LatLng lati = new LatLng(coordx, coordy);
        //verAlertaDeUsu(lati);
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

        permissionsToRequest = findUnAskedPermissions(permissions);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (permissionsToRequest.size() > 0)

                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }

        locationTrack = new tracks(MapsActivity.this,getIntent().getStringExtra("alertaid"),getIntent().getStringExtra("idusu"));


        if (locationTrack.canGetLocation()) {


            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();

            // Toast.makeText(getApplicationContext(), "Longitude:" + Double.toString(longitude) + "\nLatitude:" + Double.toString(latitude), Toast.LENGTH_SHORT).show();
        } else {

            locationTrack.showSettingsAlert();
        }

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        //JSONArray usu = new JSONArray(getIntent().getStringExtra("usu"));
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
    public void pintarTrack(LatLng pos,LatLng ant,boolean a){
        if(mMap!=null) {
            if(a)
                mMap.addPolyline(new PolylineOptions().add(pos, ant).width(5).color(Color.RED));
            else
                mMap.addPolyline(new PolylineOptions().add(pos, ant).width(5).color(Color.BLUE));
        }
    }

    public void centrarAlerta(LatLng pos,String radio){
        CameraPosition cp = CameraPosition.builder().target(pos).zoom(20-Float.parseFloat(radio)).bearing(0).tilt(0).build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp), 3000, null);
        mMap.addMarker(new MarkerOptions().position(pos));
        Log.d("PAOSPOASDPPASODPASODOAP",radio);
        mMap.addCircle(new CircleOptions().center(pos).strokeColor(Color.GREEN)
                .strokeWidth(2)
                .fillColor(Color.argb(100,106,255,0))
                .radius(Double.parseDouble(radio)));
    }
    public void getalerta(){
        String infoalerta = "http://10.0.2.2:3000/veralerta";
        StringRequest llamadaget = new StringRequest(Request.Method.POST, infoalerta, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject alerta = null;
                try {
                    Log.d("jijioooooo response",response);
                    alerta = new JSONObject(response);
                    JSONObject doc = new JSONObject(alerta.getString("doc"));
                    String coordx = doc.getString("coordx");
                    String coordy = doc.getString("coordy");
                    String radio = doc.getString("rango");
                    Log.d("PAPPAPgfhfgdhDPAPDAPSP",coordx);
                    Log.d("PAPPAPDPAPDAPSP",coordy);
                    Log.d("PAPPAPDPAPDAPSP",radio);
                    LatLng l = new LatLng(Double.parseDouble(coordx),Double.parseDouble(coordy));
                    centrarAlerta(l,radio);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                String id = getIntent().getStringExtra("alertaid");
                Log.d("dato post", id);
                params.put("id", id);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(llamadaget);
    }




    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MapsActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

}
