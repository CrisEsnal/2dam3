package com.example.rescates;

import android.location.Location;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class Model extends ViewModel {



    private final MutableLiveData<Location> selected = new MutableLiveData<Location>();

    public void selectLocation(Location item) {
        selected.setValue(item);
    }

    public LiveData<Location> getSelectedLocation() {
        return selected;
    }


}
