package com.example.rescates;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import android.widget.LinearLayout.LayoutParams;
import java.util.HashMap;
import java.util.Map;

public class MenuPrincipal  extends AppCompatActivity{
    LinearLayout.LayoutParams layoutparamsCard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuprincipal);
        getalertas();

        final String docki = getIntent().getStringExtra("usu");
        Log.e("PAPAPUUUUUUU",docki.toString());

        Button infoalertasus = findViewById(R.id.info);
        infoalertasus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject doc = new JSONObject(docki);
                    JSONObject usu = new JSONObject(doc.getString("doc"));
                    Log.d("uweriuiruiretyweruity",usu.toString());
                    String idusu = usu.getString("_id");
                    ventanainfousu(idusu);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public void ventanainfousu(final String id){
        String buscarusu = "http://10.0.2.2:3000/buscarusuid";
        StringRequest llamadaPost = new StringRequest(Request.Method.POST, buscarusu, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject doc = new JSONObject(response);
                    JSONObject usu = new JSONObject(doc.getString("doc"));
                    String alertasuscritaid = usu.getString("alertasuscritaid");
                    if(alertasuscritaid.equals("0")){
                        Toast t = Toast.makeText(getApplicationContext(), "No estas suscrito a ninguna alerta",Toast.LENGTH_SHORT);
                        t.show();
                    }else{
                        Intent alertasus = new Intent(MenuPrincipal.this,InfoAlertaSuscrita.class);
                        alertasus.putExtra("alertaid",alertasuscritaid);
                        alertasus.putExtra("idUsuario",usu.getString("_id"));
                        alertasus.putExtra("usu",usu.toString());
                        alertasus.putExtra("nombreusu",usu.getString("usuario"));
                        startActivity(alertasus);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                params.put("id", id);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(llamadaPost);

    }


    public void getalertas(){
        String buscarusu = "http://10.0.2.2:3000/veralertas";
        StringRequest llamadaget = new StringRequest(Request.Method.GET, buscarusu, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject alerta = null;
                JSONArray jsonarray = null;
                try {
                    jsonarray = new JSONArray(response);
                    for(int i = 0; i<jsonarray.length();i++){
                        alerta = new JSONObject(jsonarray.get(i).toString());
                        crearCard(alerta);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        )
        {
        };
        Volley.newRequestQueue(this).add(llamadaget);
    }

    public void crearCard(final JSONObject alerta) throws JSONException{
        Context context = getApplicationContext();
        LinearLayout linear = findViewById(R.id.linearCartas);

        CardView card = new CardView(context);
        card.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                try{
                infoAlerta(alerta,v);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        //LinearLayout linear2 = new LinearLayout(context);
        LayoutParams lparams = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
        lparams.setMargins(20,20,20,20);
        card.setLayoutParams(lparams);
        card.setCardBackgroundColor(Color.rgb(25, 100, 73));

        LinearLayout carta = new LinearLayout(context);
        LayoutParams cartaparams = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
        carta.setOrientation(LinearLayout.VERTICAL);
        carta.setLayoutParams(cartaparams);
        TextView a = new TextView(context);
        TextView b = new TextView(context);
        a.setTextColor(Color.WHITE);
        b.setTextColor(Color.WHITE);
        a.setText("Nombre de alerta: "+alerta.getString("nombreAlerta"));
        b.setText("Descripcion: "+ alerta.getString("descripcion"));
        carta.addView(a);
        carta.addView(b);
        card.addView(carta);

        linear.addView(card);
    }
    public void infoAlerta(JSONObject alerta, View v) throws JSONException{
        String infoUsu = getIntent().getStringExtra("usu");
        Intent inforalerta = new Intent(v.getContext(),infoAlertaSeleccionada.class);
        inforalerta.putExtra("idbusqueda",alerta.getString("_id"));
        inforalerta.putExtra("nombreusu",infoUsu);
        startActivity(inforalerta);
    }




}
