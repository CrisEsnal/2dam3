package com.example.rescates;

public class Mensaje {

    private String nombre;
    private String mensaje ;
    private String idalerta;

    public Mensaje(){


    }
    public Mensaje(String nickname, String message, String idalerta) {
        this.nombre = nickname;
        this.mensaje = message;
        this.idalerta = idalerta;
    }

    public String getNickname() {
        return nombre;
    }

    public void setNickname(String nickname) {
        this.nombre = nickname;
    }

    public String getMessage() {
        return mensaje;
    }

    public void setMessage(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getIdGrupo() {
        return idalerta;
    }

    public void setIdGrupo(String idGrupo) {
        this.idalerta = idGrupo;
    }
}
