package com.example.rescates;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

import static android.widget.Toast.makeText;

public class Registro extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);

        Button reg = findViewById(R.id.crear);

        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText nombre = findViewById(R.id.nombre);
                EditText contra = findViewById(R.id.contra);
                RegistroPost(nombre.getText().toString(),contra.getText().toString());
            }
        });
    }

    public void RegistroPost(final String usuario, final String contra){
        String crearusu = "http://10.0.2.2:3000/crearusu";
        StringRequest llamadaPost = new StringRequest(Request.Method.POST, crearusu, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast t = Toast.makeText(getApplicationContext(), "Se ha creado el usuario",Toast.LENGTH_SHORT);
                t.show();
                finish();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast t = Toast.makeText(getApplicationContext(), "No se ha podido crear el usuario (Usuario ya creado)",Toast.LENGTH_SHORT);
                        t.show();
                        error.printStackTrace();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                params.put("nombre", usuario);
                params.put("contra", contra);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(llamadaPost);

    }
}
