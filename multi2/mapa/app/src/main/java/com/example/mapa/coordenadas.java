package com.example.mapa;

public class coordenadas {
    private int latitud;
    private int longitud;

    public int getLatitud() {
        return latitud;
    }

    public void setLatitud(int latitud) {
        this.latitud = latitud;
    }

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    public coordenadas(){

    }
    public coordenadas(int lat, int lon){
        latitud = lat;
        longitud = lon;

    }
}
