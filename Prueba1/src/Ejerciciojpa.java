import javax.persistence;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Empleado;

public class Ejerciciojpa {
	public static EntityManager crearManager() {
		EntityManager entityManager;
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("Prueba1");
		entityManager = emfactory.createEntityManager();
		return entityManager;
		}
	@SuppressWarnings({ "unchecked", "unused" })
	public static void main(String[] args) {
		int menu = 0;
		Query query;
		EntityManager em = crearManager();
		Scanner sc = new Scanner(System.in);
		while(menu<6) {
			System.out.println("Menu");
			System.out.println("1.-Listar todos los empleados de la tabla Empleados");
			System.out.println("2.-Mostar los distinto apellidos que haya en la tabla empleados ordenados");
			System.out.println("3.-Obtener los datos del empleado con mayor salario");
			System.out.println("4.-Realizar busqueda de empleados por apellido");
			menu = sc.nextInt();
			switch(menu) {
				case 1:
					query = em.createNamedQuery("Empleado.findAll");
					//query = em.createQuery("SELECT e FROM Empleado e");
					List<Empleado> l = query.getResultList();
					for(Empleado e : l) {
						System.out.println("Nombre: "+e.getEmpNo()+" Apellido:"+e.getApellido()+" Salario:"+e.getSalario()+" ");
					}
					break;
				case 2:
					query = em.createQuery("SELECT e.apellido FROM Empleado e ORDER BY e.apellido DESC");
					List<String> a = query.getResultList();
					for(int i = 0; i<a.size();i++)
					System.out.println("Apellido: "+a.get(i));
					break;
				case 3:
					query = em.createQuery("SELECT e FROM Empleado e WHERE e.salario LIKE (SELECT MAX(e.salario) FROM Empleado e)");
					Empleado emp = (Empleado) query.getSingleResult();
					System.out.println("Nombre: "+emp.getEmpNo()+" Apellido:"+emp.getApellido()+" Salario:"+emp.getSalario()+" ");
					break;
				case 4:
					System.out.println("Introduce el apellido");
					sc.nextLine();
					String ape = sc.nextLine();
					query = em.createQuery("SELECT e FROM Empleado e WHERE e.apellido LIKE '"+ape+"'");
					List<Empleado> li = query.getResultList();
					for(Empleado e : li) {
						System.out.println("Nombre: "+e.getEmpNo()+" Apellido:"+e.getApellido()+" Salario:"+e.getSalario()+" ");
					}
				
			}
		}
	}
}
