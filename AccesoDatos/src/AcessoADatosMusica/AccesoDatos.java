package AcessoADatosMusica;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


public class AccesoDatos {
	static String url2 = "jdbc:sqlite:.\\music.db";
	public static Connection abrirCon() {
		Connection conexion = null;
		try {
			conexion = DriverManager.getConnection(url2);
			conexion.setAutoCommit(false);
		}catch(SQLException e){
			e.printStackTrace();
		}
		return conexion;
	}
	

	public static void crearTodo() {
		try{
			Connection conn = abrirCon();
			Statement st = conn.createStatement();
			//String sql = "DROP DATABASE IF EXISTS music;";
			//String sql2 = "CREATE DATABASE music"; intent� hacer como en mysql pero despues de investigar me di cuenta que no se puede hacer asi en sqlite
			File archivo = new File(".\\music.db");
			String sql3 = "DROP TABLE IF EXISTS artista;CREATE TABLE artista (id INTEGER   PRIMARY KEY AUTOINCREMENT, nombre    VARCHAR (20) UNIQUE,apellido  VARCHAR (20),localidad VARCHAR (20));";
			String sql5 = "DROP TABLE IF EXISTS disco;CREATE TABLE disco (id INTEGER   PRIMARY KEY AUTOINCREMENT,nombre VARCHAR (50),fecha_publi DATE,id_artista  INTEGER (3)  REFERENCES artista (id) NOT NULL);";

		//st.executeUpdate(sql);
		//st.executeUpdate(sql2);
		st.executeUpdate(sql3);
		st.executeUpdate(sql5);
		conn.commit();
		conn.close();
		System.out.println("Se ha creado la base de datos");
		}catch(Exception e){
			System.out.println("No se ha podido crear la base de datos");
		}
		
	}
	public static void insertarDisco(Disco disco) throws SQLException {
		String sql = "INSERT INTO disco VALUES(null,'"+disco.getNombre()+"','"+disco.getFecha_publi()+"',"+disco.getId_artista()+");";
		System.out.println(sql);
		Connection conn = abrirCon();
		try {
			
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			conn.commit();
			System.out.println("-------------Disco introducido-------------");
		} catch (SQLException e) {
			conn.rollback();
			System.out.println("Error al introducir el disco");
		}
		conn.close();
	}
	public static void insertarArtista(Artista artista) throws SQLException {
		String sqlInsertarArtista = "INSERT INTO artista (nombre,apellido,localidad) VALUES (?,?,?)";
		Connection con = abrirCon();
		try {
			Statement stat = con.createStatement();
			PreparedStatement ps = con.prepareStatement(sqlInsertarArtista);
			ps.setString(1, artista.getNombre());
			ps.setString(2, artista.getApellido());
			ps.setString(3, artista.getLocalidad());
			ps.executeUpdate();
			con.commit();
			System.out.println("Se ha guardado el artista");
		} catch (SQLException e) {
			con.rollback();
			System.out.println("Error al insertar artista, intenta de nuevo");
		}
		con.close();
	}

	public static void eliminarArtista(int id) throws SQLException {
		String sqlBorrarArtista = "DELETE FROM artista WHERE id = "+id+";";
		Boolean borrar = false;
		Scanner sc = new Scanner(System.in);
		while(!borrar) {
			System.out.println("�Est�s seguro que quieres borrar el artista con el id"+id+"?");
			System.out.println("S/N");
			if(sc.nextLine().equalsIgnoreCase("s")) {
				borrar = true;
				Connection conn = abrirCon();
				try {
					
					Statement stat = conn.createStatement();
					stat.executeUpdate(sqlBorrarArtista);
					conn.commit();
					System.out.println("Artista Eliminado satisfactoriamente");
					conn.commit();
					conn.close();
				}catch(SQLException e) {
					conn.rollback();
					System.out.println("El artista no se ha podido eliminar, compreba que hayas metido el id correcto");
					e.printStackTrace();
				}
			}else {
				System.out.println("No se ha borrado el artista");
				borrar = true;
			}
		}
	}
	public static void datos(String nombre) throws SQLException {
		String sql = "SELECT * FROM artista where nombre = '"+nombre+"';";
		Connection conn = abrirCon();
		try{
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			System.out.println("------------Datos------------");
			System.out.println("ID: "+rs.getInt("id"));
			System.out.println("Nombre: "+rs.getString("nombre"));
			System.out.println("Apellido: "+rs.getString("apellido"));
			System.out.println("Localidad: "+rs.getString("localidad"));
			System.out.println("-----------Discos------------");
			
			
			int id = rs.getInt("id");

			Statement st2 = conn.createStatement();
			String sql2 = "SELECT * FROM disco where id_artista=" +id +";";
			
			
			ResultSet rs2 = st2.executeQuery(sql2);
			ResultSetMetaData rsmd = rs2.getMetaData();
			int col = rsmd.getColumnCount(); 			
			
			while(rs2.next()) { //Se recorren los discos y se visualizan
				System.out.println("------------ Disco  artista ------------");
					for(int i = 1 ; i <= col; i++){
						
						System.out.println(rsmd.getColumnName(i) + ": " + rs2.getString(i));
					}
				System.out.println("-------------------------------------------");	
			}

		}catch(SQLException e) {
			System.out.println("***************");
			System.out.println("Artista erroneo");
			System.out.println("***************");
		}
		conn.close();
	}
	public static void modificar(Artista modartista) throws SQLException {
		//Funcion que sirve para modificar la info de un artista, se le introduce un objeto artista y un id que es por el que se va a modificar
		Connection conn = abrirCon();
		try {
			
			Statement st = conn.createStatement();		
			String sql = "SELECT * FROM artista where id=" + modartista.getId();
			ResultSet rs = st.executeQuery(sql);
			String sql2 = "UPDATE artista SET nombre = '"+modartista.getNombre()+"', apellido= '"+modartista.getApellido()+"', localidad = '"+modartista.getLocalidad()+"' WHERE id =" + modartista.getId();
			Statement st2 = conn.createStatement();
			st2.executeUpdate(sql2);
			conn.commit();
			conn.close();
			System.out.println("Artista modificado");
		}catch(SQLException e) {
			conn.rollback();
			e.printStackTrace();
			System.out.println("No se ha podido modificar la informacion del artista");
		}
		
	}
	
}
