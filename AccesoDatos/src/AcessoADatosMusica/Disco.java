package AcessoADatosMusica;

import java.sql.Date;

public class Disco {
	private int id;
	private String nombre;
	private String fecha_publi;
	private int id_artista;
	public Disco(int id, String nombre, String fecha_publi, int id_artista) {
		this.id = id;
		this.nombre = nombre;
		this.fecha_publi = fecha_publi;
		this.id_artista = id_artista;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFecha_publi() {
		return fecha_publi;
	}
	public void setFecha_publi(String fecha_publi) {
		this.fecha_publi = fecha_publi;
	}
	public int getId_artista() {
		return id_artista;
	}
	public void setId_artista(int id_artista) {
		this.id_artista = id_artista;
	}
	
}
