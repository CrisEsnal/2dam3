package AcessoADatosMusica;
import java.sql.*;
import java.util.Scanner;
import AcessoADatosMusica.AccesoDatos;
public class Musica {
	
	
	@SuppressWarnings("null")
	public static void main(String[]args) throws SQLException {
		int menu = 0;
		Scanner sc = new Scanner(System.in);
		while(menu!=7) {
			System.out.println("************************");
			System.out.println("1.-Crear base de datos");
			System.out.println("2.-Insertar Artista");
			System.out.println("3 -Insertar nuevo disco");
			System.out.println("4.-Consultar Artista");
			System.out.println("5.-Modificar nombre de Artista");
			System.out.println("6.-Eliminar Artista");
			System.out.println("7.-Salir de la aplicacion");
			
			System.out.println("************************");
			menu = sc.nextInt();
			switch(menu) {
				case 1:
					sc.nextLine();
					System.out.println("Si hay base de datos existente se va a eliminar �Estas seguro? s/n");
					String res = sc.nextLine();
					if(res.equalsIgnoreCase("s")) {
						AccesoDatos.crearTodo();
					}else {
						System.out.println("No ha pasado nada");
					}
					break;
				case 2:
					sc.nextLine();
					System.out.println("Introduce el nombre");
					String nombre = sc.nextLine();
					System.out.println("Introduce el apellido");
					String ape = sc.nextLine();
					System.out.println("Introduce la localidad");
					String loc = sc.nextLine();
					AccesoDatos.insertarArtista(new Artista(0,nombre,ape,loc));
					break;
				case 3:
					sc.nextLine();
					System.out.println("Introduce el nombre del disco");
					String nombreDisco = sc.nextLine();
					System.out.println("Introduce la fecha de publicaci�n ejemplo: 02/10/2012");
					String date = sc.nextLine();
					System.out.println("Introduce el id del artista creador");
					int id = sc.nextInt();
					AccesoDatos.insertarDisco(new Disco(0,nombreDisco,date,id));
					break;
				case 4:
					sc.nextLine();
					System.out.println("Escribe el nombre");
					String nombreBusqueda = sc.nextLine();
					AccesoDatos.datos(nombreBusqueda);
					break;
				case 5:
					sc.nextLine();
					//Introduccion de datos
					System.out.println("Introduce el id del artista a modificar");
					int idartista = sc.nextInt();
					System.out.println("Introduce el nuevo nombre");
					sc.nextLine();
					String nombreNuevo = sc.nextLine();
					System.out.println("Introduce el nuevo apellido");
					String apellidoNuevo = sc.nextLine();
					System.out.println("Introduce la nueva localidad");
					String nuevaLoc = sc.nextLine();
					AccesoDatos.modificar(new Artista(idartista,nombreNuevo, apellidoNuevo, nuevaLoc));
					break;
				case 6:
					sc.nextLine();
					System.out.println("Introduce el id del artista a eliminar");
					int idDel = sc.nextInt();
					AccesoDatos.eliminarArtista(idDel);
					
			}
		}
	}
}
