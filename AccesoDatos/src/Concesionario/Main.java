package Concesionario;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import java.io.*;
public class Main {
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		//Get Document Builder
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		 
		//Build Document
		Document document = builder.parse(new File("C:\\prueba2dam3\\concesionario.xml"));
		 
		//Normalize the XML Structure; It's just too important !!
		document.getDocumentElement().normalize();
		 
		//Here comes the root node
		Element root = document.getDocumentElement();
		System.out.println(root.getNodeName());
		 
		//Get all employees
		NodeList nList = document.getElementsByTagName("coche");
		System.out.println("============================");
		 
		for (int temp = 0; temp < nList.getLength(); temp++)
		{
		 Node node = nList.item(temp);
		 System.out.println("");    //Just a separator
		 if (node.getNodeType() == Node.ELEMENT_NODE)
		 {
		    //Print each employee's detail
		    Element eElement = (Element) node;
		    System.out.println("Id de coche : "    + eElement.getAttribute("id"));
		    System.out.println("Marca : "  + eElement.getElementsByTagName("marca").item(0).getTextContent());
		    System.out.println("Modelo : "   + eElement.getElementsByTagName("modelo").item(0).getTextContent());
		    System.out.println("Cilindrada : "    + eElement.getElementsByTagName("cilindrada").item(0).getTextContent());
		 }
		 System.out.println("===========================");
		}
	}
}
