package AccesoFicherosJSON;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import javax.xml.parsers.FactoryConfigurationError;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
public class Main {
	public static JSONParser parser = new JSONParser();
	public static void listarCLientes(String ruta){
		//Function que lista todos los clientes del json
		try {
			//Lectura del archivo
			FileReader file = new FileReader(ruta);
			//Parseo a JSONArray de los objetos 
			JSONArray listaUsus = (JSONArray) parser.parse(file);
			System.out.println("--------------Lista--------------");
			//hago un for para recorrer todos los objetos del array y los imprimo por pantalla
			for(int i = 0;i<listaUsus.size();i++) {
				JSONObject cliente = (JSONObject) listaUsus.get(i);
				
				System.out.println("|  ID: "+cliente.get("-id"));
				System.out.println("|  Nombre: "+cliente.get("nombre"));
				System.out.println("|  Edad: "+cliente.get("edad"));
				System.out.println("|  Poblacion: "+cliente.get("poblacion"));
				System.out.println("---------------------------------");
			}
			//cierro el stream del archivo
			file.close();

		}catch(IOException | ParseException e)
		{

			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	public static void crearCliente(String ruta,Cliente cli) {
		//function que crea un cliente nuevo
		try {
			//conexion al archivo json
			FileReader file2 = new FileReader(ruta);
			//parseo a JSONArray desde el archivo json
			JSONArray listaUsuariosCreacion = (JSONArray) parser.parse(file2);
			//Cojo el ultimo cliente y le saco el id para sumarle 1 al id
			JSONObject cliente = (JSONObject) listaUsuariosCreacion.get(listaUsuariosCreacion.size()-1);
			
			int id = Integer.parseInt((String) cliente.get("-id"));
			id++;
			
			//creo el nuevo cliente y le a�ado los datos
			JSONObject o = new JSONObject();
			o.put("-id", Integer.toString(id));
			o.put("nombre",cli.getNombre());
			o.put("edad", cli.getEdad());
			o.put("poblacion", cli.getPoblacion());
			//a�ado el objeto creado al final del array de objetos
			listaUsuariosCreacion.add(o);
			//stream de escribir en el fichero
			FileWriter filew2 = new FileWriter(ruta);
			//escribo el archivo con el array
				filew2.write(listaUsuariosCreacion.toJSONString());
				filew2.flush();
				filew2.close();
				file2.close();
			//cierro streams
			
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}
	public static Cliente consultarCliente(String ruta,String idConUsu) {
		//Funcion que hace una consulta al id de cliente marcado
		Cliente cli = new Cliente();
		try {
			//conexion al archivo json
			FileReader file3 = new FileReader(ruta);
			//parseo a JSONArray desde el archivo json
			JSONArray listaUsuariosConsulta = (JSONArray) parser.parse(file3);
			//recorro el array de usuarios en busca del usuario con el id introducido
			for(int i=0;i<listaUsuariosConsulta.size();i++) {
				JSONObject clienteConsulta = (JSONObject) listaUsuariosConsulta.get(i);
				if(clienteConsulta.get("-id").equals(idConUsu)) {
					cli.setNombre(clienteConsulta.get("nombre").toString());
					cli.setEdad(clienteConsulta.get("edad").toString());
					cli.setPoblacion(clienteConsulta.get("poblacion").toString());
				}
			}
			//cierre de conexion
			file3.close();
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		return cli;
	}
	@SuppressWarnings("unchecked")
	public static boolean ModUsu(String ruta,String idModUsu,Scanner sc){
		//funcion que modifica el nombre de un usuario
		
		Boolean entrada = false;
		
		try {
			//conexion al archivo json
			FileReader file4 = new FileReader(ruta);
			//parseo a JSONArray desde el archivo json
			JSONArray listaUsuariosModificacion = (JSONArray) parser.parse(file4);
			//Busco el cliente mediante un id,una vez encontrado, paso a preguntar cual es el nuevo nombre
			for(int i=0;i<listaUsuariosModificacion.size();i++) {
				JSONObject clienteMod = (JSONObject) listaUsuariosModificacion.get(i);
				if(clienteMod.get("-id").equals(idModUsu)) {
					System.out.println("Introduce el nombre ");
					String nombreNuevo = sc.nextLine();
					System.out.println("El nombre de idModUsu cliente es: " + clienteMod.get("nombre") + " y se va a cambiar por: " + nombreNuevo);
					clienteMod.replace("nombre", nombreNuevo);
					listaUsuariosModificacion.set(i, clienteMod);
					entrada = true;
				}
			}
			//abro un stream de escritura de datos
			FileWriter filew4 = new FileWriter(ruta);
			//escribo los datos
			filew4.write(listaUsuariosModificacion.toJSONString());
			filew4.flush();
			filew4.close();
			file4.close();
			//cierro el stream
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		
		return entrada;
		
	}
	public static Boolean delUsu(String ruta, String idDel,Scanner sc) {
		//funci�n que elimina un usuario seg�n id
		Boolean entrada = false;
		try {
			//conexion al archivo json
			FileReader file5 = new FileReader(ruta);
			//parseo a JSONArray desde el archivo json
			JSONArray listaUsuariosDel = (JSONArray) parser.parse(file5);
			//busqueda mediante id, una vez encontrado, pasa a preguntar si est�s seguro de la eliminaci�n del cliente
			for(int i=0;i<listaUsuariosDel.size();i++) {
				JSONObject clienteDel = (JSONObject) listaUsuariosDel.get(i);
				if(clienteDel.get("-id").equals(idDel)) {
					System.out.println("Vas a eliminar el siguiente usuario: " + idDel + ". �Estas seguro?");
					String son = sc.nextLine().toLowerCase();
					if(son.equals("si")) {
						listaUsuariosDel.remove(i);
						entrada = true;
					}
				}
			}
			//abro un stream de escritura de datos
			FileWriter filew5 = new FileWriter(ruta);
			//escribo los datos
			filew5.write(listaUsuariosDel.toJSONString());
			filew5.flush();
			filew5.close();
			
			//cierro el stream
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		return entrada;
		
	}
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public static void main(String[]args) throws FileNotFoundException, IOException, ParseException{
		int menu = 0;
		String ruta = "";
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Antes de empezar, configuremos donde va a estar la ruta!\n"
				+ "Recuerda que la ruta tiene que ser absoluta y con /");
		ruta = sc.nextLine();
		while(menu!=6) {
			System.out.println("************************");
			System.out.println("Acceso a ficheros JSON");
			System.out.println("------------------------");
			System.out.println("1.-Listar clientes");
			System.out.println("2.-Crear cliente");
			System.out.println("3.-Consultar cliente");
			System.out.println("4.-Modificar nombre de cliente");
			System.out.println("5.-Eliminar cliente");
			System.out.println("6.-Salir de la aplicacion");
			System.out.println("************************");
			System.out.println("------------------------");
			menu = sc.nextInt();
			
			switch(menu) {
			case 1:

				listarCLientes(ruta);
				break;
			case 2:
				sc.nextLine();
				System.out.println("Introducci�n de datos");
				System.out.println("Introduce el nombre:");
				String nombre = sc.nextLine();
				System.out.println("Introduce la edad");
				String edad = sc.nextLine();
				System.out.println("Introduce la poblacion:");
				String pob = sc.nextLine();
				Cliente cli = new Cliente(nombre,edad,pob);
				crearCliente(ruta,cli);
				break;
			case 3:
				System.out.println("Introduce el id del usuario que quieres consultar:");
				sc.nextLine();
				String idConUsu = sc.nextLine();
				Cliente clienteConsultado = consultarCliente(ruta, idConUsu);
				System.out.println("*******************");
				System.out.println("Nombre del cliente:" + clienteConsultado.getNombre());
				System.out.println("Edad del cliente: " + clienteConsultado.getEdad());
				System.out.println("Poblacion del cliente: "+ clienteConsultado.getPoblacion());
				break;
			case 4:
				System.out.println("Introduce el id del usuario que quieres modificar:");
				sc.nextLine();
				String idModUsu = sc.nextLine();
				if(ModUsu(ruta, idModUsu,sc)) {
					System.out.println("Usuario modificado correctamente");
				}
				break;
			case 5:
				
				System.out.println("Introduce el id que quieres eliminar");
				sc.nextLine();
				String idDel = sc.nextLine();
				if(delUsu(ruta, idDel,sc)) {
					System.out.println("Usuario Eliminado");
				}
				
				break;
				
			}
		}
	}
}
