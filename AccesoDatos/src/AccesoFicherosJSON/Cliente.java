package AccesoFicherosJSON;

public class Cliente {

	private String nombre;
	private String edad;
	private String poblacion;
	public Cliente() {

		this.nombre="";
		this.edad = "";
		this.poblacion = "";
	}
	
	public Cliente(String nombre, String edad, String poblacion) {

		this.nombre = nombre;
		this.edad = edad;
		this.poblacion = poblacion;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEdad() {
		return edad;
	}
	public void setEdad(String edad) {
		this.edad = edad;
	}
	public String getPoblacion() {
		return poblacion;
	}
	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}
}
