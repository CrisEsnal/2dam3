package ConcesionarioLeer;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

public class ParseadorSAX {

    public static void main(String[] args) {
	    SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
	    try {
	        SAXParser saxParser = saxParserFactory.newSAXParser();
	        MiHandler handler = new MiHandler();
	        saxParser.parse(new File("C:\\\\prueba2dam3\\\\concesionario.xml"), handler);
	        //Obtiene la lista de coches
	        List<Coche> cocheList = handler.getCocheList();
	        //Imprime la información del coche
	        for(Coche coche : cocheList)
	            System.out.println(coche);
	    } catch (IOException | SAXException | ParserConfigurationException e) {
	        e.printStackTrace();
	    }
    }

}
