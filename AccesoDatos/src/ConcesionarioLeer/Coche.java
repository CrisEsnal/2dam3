package ConcesionarioLeer;

public class Coche {
	
	    //XML attribute id
	    private int id;
//	    /XML element modelo
	    private String modelo;	    
		//XML element marca
	    private String marca;
	    //XML element cilindrada
	    private double cilindrada;
	 
	    public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
	    public String getModelo() {
			return modelo;
		}
		public void setModelo(String modelo) {
			this.modelo = modelo;
		}
		public String getMarca() {
			return marca;
		}
		public void setMarca(String marca) {
			this.marca = marca;
		}
		public double getCilindrada() {
			return cilindrada;
		}
		public void setCilindrada(double cilindrada) {
			this.cilindrada = cilindrada;
		}

	 
	    @Override
	    public String toString() {
	        return "----------------------" + "\n" + "Id: " + this.id + "\n" + "Modelo: " + this.modelo +  "\n" + "Marca: " + this.marca + "\n" + "Cilindrada: " + this.cilindrada ;
	    }
	
}
