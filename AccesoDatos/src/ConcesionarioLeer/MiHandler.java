package ConcesionarioLeer;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class MiHandler extends DefaultHandler {

	// Lista para capturar los objetos tipo coche
	private List<Coche> cocheList = null;
	private Coche coche = null;
	private StringBuilder data = null;
	
	public List<Coche> getCocheList() {
		return cocheList;
	}

	boolean bModelo = false;
	boolean bMarca = false;
	boolean bCilindrada = false;

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		if (qName.equalsIgnoreCase("coche")) {
			// crea un nuevo coche
			String id = attributes.getValue("id");
			// inicializa el objeto coche y le atribuye un id
			coche = new Coche();
			coche.setId(Integer.parseInt(id));
			// inicializa la lista
			if (cocheList == null) cocheList = new ArrayList<>();
		} else if (qName.equalsIgnoreCase("modelo")) {
			bModelo = true;
		} else if (qName.equalsIgnoreCase("marca")) {
			bMarca = true;
		} else if (qName.equalsIgnoreCase("cilindrada")) {
			bCilindrada = true;
		}
		// create the data container
		data = new StringBuilder();
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (bModelo) {
			// elemento modelo, se asigna el valor del modelo
			coche.setModelo(data.toString());
			bModelo = false;
		} else if (bMarca) {
			coche.setMarca(data.toString());
			bMarca = false;
		} else if (bCilindrada) {
			coche.setCilindrada(Double.parseDouble(data.toString()));
			bCilindrada = false;
		}
		
		if (qName.equalsIgnoreCase("coche")) {
			// se a�ade el coche a la lista
			cocheList.add(coche);
		}
	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		data.append(new String(ch, start, length));
	}
}