package ObjectInputStream_ObjectOutputStream;

import java.io.Serializable;

public class Coche implements Serializable{
	private String matricula;
	private String marca;
	private double deposito;
	public Coche(String matricula, String marca, double deposito) {
		this.matricula = matricula;
		this.marca = marca;
		this.deposito = deposito;
	}
	
	public Coche(String matricula, String marca) {
		this.matricula = matricula;
		this.marca = marca;
		this.deposito = 0;
	}

	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public double getDeposito() {
		return deposito;
	}
	public void setDeposito(double deposito) {
		this.deposito = deposito;
	}
}

