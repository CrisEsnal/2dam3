package ObjectInputStream_ObjectOutputStream;
import java.io.*;

public class Ejercicio2 {
	public static void main(String[]args) throws IOException, ClassNotFoundException {
		File ficher = new File("D:/2dam3/AccesoDatos/pruebas/prueba2.txt");
		FileInputStream fileins = new FileInputStream(ficher);
		ObjectInputStream datoIS = new ObjectInputStream(fileins);
		
		try {
			while(true) {
				Coche coche = (Coche) datoIS.readObject();
				System.out.println("Matricula: " + coche.getMatricula());
				System.out.println("Marca: " + coche.getMarca());
				System.out.println();
			}
		}catch(EOFException eo) {
		}
		datoIS.close();
	}
}