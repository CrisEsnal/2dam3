package ClasesFileReaderFileWriter;

import java.io.*;

public class Ejercicio1 {
	public static void main(String[]args)throws IOException {
		
		File fichero = new File("D:/2dam3/AccesoDatos/pruebas/prueba.txt");
		FileReader fic = new FileReader(fichero);
		int i;
		while ((i = fic.read()) != -1)
			System.out.print((char) i);
		fic.close();
	}
}
