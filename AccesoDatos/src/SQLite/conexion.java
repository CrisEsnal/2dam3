package SQLite;
import java.sql.*;
import java.util.Scanner;

import org.sqlite.core.DB;
public class conexion {
	static String url = "jdbc:sqlite:D:\\2dam3\\AccesoDatos\\sqlite\\sqlite-tools-win32-x86-3300000\\empleados.db";
	static String url2 = "jdbc:mysql://localhost/ejemplo";
	static String sqlSelect = "SELECT * FROM departamentos";
	public static void insertarEmpleados() throws SQLException {
		String sql = "INSERT INTO empleados VALUES(11,\"esnal\",1000.00,10)";
		Connection conn = abrirCon();
		Statement statement = conn.createStatement();
		int result = statement.executeUpdate(sql);
	}
	
	public static void insertarEmpleadoscreateStatement(Empleado empleado) throws SQLException {
		String sql = "INSERT INTO empleados VALUES(?,?,?,?)";
		Connection conn = abrirCon();
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, empleado.getEmp_no());
		ps.setString(2, empleado.getApellido());
		ps.setFloat(3, empleado.getSalario());
		ps.setInt(4, empleado.getDept_no());
		ps.executeUpdate();
	}
	
	public static void crearTablaEmpleados() throws SQLException{
		Connection conn = abrirCon();
		try {
			
			Statement statement = conn.createStatement();
			
			String sql = "CREATE TABLE empleados(emp_no TINYINT(2) NOT NULL PRIMARY KEY, apellido VARCHAR(10),salario FLOAT(6,2),dept_no TINYINT(2) NOT NULL REFERENCES departamentos(dept_no));";
			int result = statement.executeUpdate(sql);
		}catch(SQLException e) {
			cerrarCon(conn);
		}
	}
	public static void consultarMetadata(Connection conexion) throws SQLException {
		
		DatabaseMetaData dbmd = conexion.getMetaData();
		System.out.println("Nombre de producto:"+dbmd.getDatabaseProductName()+"\n Version del producto: "+dbmd.getDatabaseProductVersion()+"\n Nombre del driver: "+dbmd.getDriverName());
		
	}
	public static Connection abrirCon() throws SQLException {
		Connection conexion = null;

		conexion = DriverManager.getConnection(url2,"root","root");
		
		System.out.println("La conexion a la base de datos a sido exitosa");
		return conexion;
	}
	
	public static void cerrarCon(Connection conexion) {
		try {
			if(conexion !=null) {
				conexion.close();
			}
		}catch(SQLException e) {
			System.out.println(e);
		}
	}
	
	public static void main(String[]args) {
				
		try {
			crearTablaEmpleados();
			
			Empleado emp = new Empleado();
			Scanner sc = new Scanner(System.in);
			System.out.println("Inserta id Empleado");
			emp.setEmp_no(sc.nextInt());
			System.out.println("Insertar Apellido");
			sc.nextLine();
			emp.setApellido(sc.nextLine());
			System.out.println("Introduce salario");
			emp.setSalario(sc.nextFloat());
			System.out.println("Introduce departamento");
			sc.nextLine();
			emp.setDept_no(sc.nextInt());
			insertarEmpleadoscreateStatement(emp);
			Connection conexion = abrirCon();
			
			Statement statement = conexion.createStatement();
			ResultSet rs = statement.executeQuery(sqlSelect);
			//System.out.println(rs.getMetaData());
			while(rs.next()) {
				
				System.out.println("Nombre = "+ rs.getString("dnombre"));
				System.out.println("ID = "+ rs.getString("dept_no"));
			}
			consultarMetadata(conexion);
			cerrarCon(conexion);
		}catch(SQLException ex) {
			System.out.println(ex);
		}
	
	}
}
