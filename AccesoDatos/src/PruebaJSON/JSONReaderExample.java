package PruebaJSON;
import java.io.FileReader; 
import java.util.Iterator; 
import java.util.Map; 

import org.json.simple.JSONArray; 
import org.json.simple.JSONObject; 
import org.json.simple.parser.*; 

public class JSONReaderExample {

	public static void main(String[] args)throws Exception {
		Object obj = new JSONParser().parse(new FileReader("D:\\2dam3\\AccesoDatos\\pruebas\\clientes.json"));
		
		JSONObject jobj =(JSONObject) obj;
	
		String nombre = (String) jobj.get("firstname");
		String apellido = (String) jobj.get("lastname");
		
		System.out.println(nombre);
		System.out.println(apellido);
	
		Long edad = (Long) jobj.get("age");
		System.out.println(edad);
		
		Map address = ((Map)jobj.get("address"));
		
		Iterator<Map.Entry> itr1 = address.entrySet().iterator();
		
		while(itr1.hasNext()) {
			Map.Entry pair = itr1.next();
			System.out.println(pair.getKey()+ " : " + pair.getValue());
		}
		
		JSONArray jarr  = (JSONArray) jobj.get("phoneNumbers");
		
		Iterator itr2 = jarr.iterator();
		
		while(itr2.hasNext()) {
			itr1 = ((Map) itr2.next()).entrySet().iterator();
			while (itr1.hasNext()) {
				Map.Entry pair = itr1.next(); 
                System.out.println(pair.getKey() + " : " + pair.getValue()); 
			}
		}
	}
}
